<?php
/**
 * Created by PhpStorm.
 * User: jvalero
 * Date: 19/05/17
 * Time: 9:35
 */

namespace Drupal\web_service_client\Plugin\migrate\source;

use ArrayIterator;
use IteratorAggregate;

class WSIter implements IteratorAggregate {

  private $items;

  public function __construct($elements = []) {
    $this->items = $elements;
  }

  public function getIterator() {
    return new ArrayIterator($this->items);
  }

}
