<?php

/**
 * Created by PhpStorm.
 * User: jvalero
 * Date: 16/05/17
 * Time: 16:35
 * https://ohthehugemanatee.org/blog/2015/05/02/how-to-build-a-new-source-for-drupal-migrate-8/
 */

/**
 * @file
 * Contains \Drupal\web_service_client\Plugin\migrate\source\web_service_views.
 */

namespace Drupal\web_service_client\Plugin\migrate\source;

use ArrayIterator;
use Drupal;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\views\Views;
use IteratorIterator;

/**
 * Source plugin for retrieving data from views.
 *
 * @MigrateSource(
 *   id = "web_service_views"
 * )
 */
class WSViews extends SourcePluginBase {

  /**
   * Information on the source fields to be extracted from the data.
   *
   * @var array[]
   *   Array of field information keyed by field names. A 'label' subkey
   *   describes the field for migration tools; a 'path' subkey provides the
   *   source-specific path for obtaining the value.
   */
  protected $fields = [];

  /**
   * Description of the unique ID fields for this source.
   *
   * @var array[]
   *   Each array member is keyed by a field name, with a value that is an
   *   array with a single member with key 'type' and value a column type such
   *   as 'integer'.
   */
  protected $ids = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    // View name and display are required.
    if (empty($this->configuration['view_name'] || empty($this->configuration['view_display']))) {
      return new MigrateException('You must declare the view name and display in your source settings.');
    }

    $this->fields = $this->fields();
    $this->ids = $this->getIds();
  }

  /**
   * Return a string representing the source view name and display.
   *
   * @return string
   *   The file path.
   */
  public function __toString() {
    return $this->configuration['view_name'] . '/' . $this->configuration['view_display'];
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [];

    if (!empty($this->configuration['fields'])) {
      $fields = $this->configuration['fields'];
    }

    return $fields;
  }


  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [];
    foreach ($this->configuration['keys'] as $delta => $value) {
      if (is_array($value)) {
        $ids[$delta] = $value;
      }
      else {
        $ids[$value]['type'] = 'string';
      }
    }
    return $ids;
  }

  /**
   * Creates and returns a filtered Iterator over the view rows.
   *
   * @return \Iterator
   *   An iterator over the source rows
   */
  protected function initializeIterator() {
    if ($this->configuration['view_name'] && $this->configuration['view_display']) {
      $migration_id = NULL;
      if (method_exists($this->idMap, 'getMigration')) {
        $migration = $this->idMap->getMigration();
        $migration_id = $migration['id'];
      }

      $results = $this->getViewRows($this->configuration['view_name'], $this->configuration['view_display']);

      Drupal::logger('wsc: ' . $migration_id)
        ->notice('Total rows to import: ' . iterator_count($results->getIterator()));
      return new IteratorIterator($results);
    }
    else {
      return new ArrayIterator($this->configuration['fields']);
    }
  }

  protected function getViewRows($name, $display) {
    // Obtain source view
    $view = Views::getView($name);

    if (is_object($view)) {
      $view->setDisplay($display);
      $view->execute();
      $fields = array_keys($view->field);
      $result = [];
      foreach ($view->result as $key => $val) {
        foreach ($fields as $field) {
          if (count($val->$field) <= 1) {
            $result[$key][$field] = empty($val->$field[0]) ? NULL : $val->$field[0];
          }
          else {
            foreach ($val->$field as $item) {
              $result[$key][$field][] = $item;
            }
          }
        }
        $result[$key]['raw'] = $val->getRaw();
      }
      if (empty($result)) {
        $message = t('The following view does not return results: @view.', ['@view' => $name]);
        Drupal::logger('wsc_migrate_plugin')->warning($message);
      }
      return new WSIter($result);
    }
    else {
      // Must return a Traversable Object for avoid errors
      $message = t('The following view does not exists: @view.', ['@view' => $name]);
      Drupal::logger('wsc_migrate_plugin')->error($message);
      return new WSIter();
    }
  }

}
