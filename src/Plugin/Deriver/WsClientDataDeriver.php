<?php

namespace Drupal\web_service_client\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\web_service_client\Entity\WsClient;

/**
 * Makes a flexible layout for each layout config entity.
 */
class WsClientDataDeriver extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definitions) {
    $services = WsClient::loadMultiple();
    foreach ($services as $service) {
      $dataTypes = $service->getDataTypes();
      if (empty($dataTypes)) {
        continue;
      }
      $service_definition = $service->getEndpoint()->getPluginDefinition();
      $namespace = '\Drupal\\' . $service_definition['provider'];
      foreach ($dataTypes as $name => $dataType) {
        $id = $service->id() . ':' . $name;
        $this->derivatives[$id] = [
          'label' => $dataType['label'],
          'provider' => 'web_service_client',
          'id' => $id,
          'list_definition_class' => $namespace . '\TypedData\WsClientListDataDefinition',
          'list_class' => $namespace . '\Plugin\DataType\WsClientItemList',
          'definition_class' => $namespace . '\TypedData\WsClientDataDefinition',
          'class' => $namespace . '\Plugin\DataType\WsClientData',
        ];

        if (!empty($dataType['field_settings']['typed_data']['definition_class'])) {
          $this->derivatives[$id]['definition_class'] = $dataType['field_settings']['typed_data']['definition_class'];
        }

        if (!empty($dataType['field_settings']['typed_data']['list_definition_class'])) {
          $this->derivatives[$id]['list_definition_class'] = $dataType['field_settings']['typed_data']['list_definition_class'];
        }

        if (!empty($dataType['field_settings']['typed_data']['class'])) {
          $this->derivatives[$id]['class'] = $dataType['field_settings']['typed_data']['class'];
        }

        if (!empty($dataType['field_settings']['typed_data']['list_class'])) {
          $this->derivatives[$id]['list_class'] = $dataType['field_settings']['typed_data']['list_class'];
        }
      }
    }

    return $this->derivatives;
  }

}
