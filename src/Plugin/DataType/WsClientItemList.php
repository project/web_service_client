<?php

namespace Drupal\web_service_client\Plugin\DataType;

use Drupal;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\Core\TypedData\TypedData;
use Drupal\web_service_client\TypedData\WsClientDataDefinition;
use Drupal\web_service_client\TypedData\WsClientListDataDefinition;

/**
 * A generic list class.
 *
 * This class can serve as list for any type of items and is used by default.
 * Data types may specify the default list class in their definition, see
 * Drupal\Core\TypedData\Annotation\DataType.
 * Note: The class cannot be called "List" as list is a reserved PHP keyword.
 *
 * @ingroup typed_data
 *
 * @DataType(
 *   id = "web_service_client_list",
 *   label = @Translation("List of items"),
 *   definition_class =
 *   "\Drupal\web_service_client\TypedData\WsClientListDataDefinition"
 * )
 */
class WsClientItemList extends ItemList {

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (!isset($values[0])) {
      $values = [$values];
    }
    parent::setValue($values, $notify);
  }

  public function getChildByTrail(string $trail, $response = NULL) {
    $trail = explode('/', $trail);
    array_shift($trail);
    $dataType = $this;
    foreach ($trail as $key => $child) {
      if ($dataType instanceof TypedData) {
        $dataType = $dataType->getChild($child, $dataType);
      }
      else {
        return NULL;
      }
    }

    if (!empty($response)) {
      $this->rebuildChildrenTypesByResponse($response);
    }

    return $dataType;
  }

  public function getChild(string $child, TypedData $type = NULL) {
    if (empty($type)) {
      $properties_data_definition = $this->definition->getItemDefinition()
        ->getPropertyDefinitions();
    }
    else {
      $properties_data_definition = $type->definition->getItemDefinition()
        ->getPropertyDefinitions();
    }

    if (!empty($properties_data_definition[$child])) {
      if (empty($this->typedDataManager)) {
        $this->typedDataManager = Drupal::typedDataManager();
      }
      $dataType = $this->typedDataManager->create($properties_data_definition[$child]);
      return $dataType;
    }
  }

  public function rebuildChildrenTypesByResponse($response) {
    return;
    $children = $response->childNodes;
    $dataType = NULL;
    $item_definition = $this->getItemDefinition();
    $properties = $item_definition->getPropertyDefinitions();
    for ($i = 0; $i < $children->length; $i++) {
      $child = $children->item($i);
      if ($child->nodeType == XML_ELEMENT_NODE) {
        $xsi_type = $child->getAttribute('xsi:type');
        if (!empty($xsi_type)) {
          list($namespace, $type) = explode(':', $xsi_type);
          $base_type = explode(':', $item_definition->getDataType());
          $base_type[2] = $type;
          $type = implode(':', $base_type);
          if ($this instanceof ItemList) {
            $type_def = Drupal::typedDataManager()
              ->createListDataDefinition($type);
          }
          else {
            $type_def = Drupal::typedDataManager()
              ->createDataDefinition($type);
          }
          $dataType = Drupal::typedDataManager()->create($type_def);
        }

        $property = $child->nodeName;
        if (!empty($properties[$property]) && ($properties[$property] instanceof WsClientDataDefinition || $properties[$property] instanceof WsClientListDataDefinition)) {
          if (!empty($dataType)) {
            $properties[$property] = $dataType->getDataDefinition();
          }
        }
      }
    }
    $item_definition->setPropertyDefinitions($properties);
    $this->definition->setItemDefinition($item_definition);
  }

}
