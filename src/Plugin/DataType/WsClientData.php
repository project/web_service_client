<?php

namespace Drupal\web_service_client\Plugin\DataType;

use Drupal;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\Plugin\DataType\ItemList;
use Drupal\Core\TypedData\Plugin\DataType\Map;
use Drupal\Core\TypedData\TypedData;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\web_service_client\TypedData\WsClientListDataDefinition;
use InvalidArgumentException;
use function array_shift;
use function is_array;

/**
 * Defines the base plugin for deriving data types for wsclient.
 *
 * Note that the class only register the plugin, and is actually never used.
 * \Drupal\Core\Field\FieldItemBase is available for use as base class.
 *
 * @DataType(
 *   id = "web_service_client",
 *   definition_class =
 *   "\Drupal\web_service_client\TypedData\WsClientDataDefinition", list_class
 *   = "\Drupal\Core\TypedData\Plugin\DataType\ItemList", list_definition_class
 *   = "\Drupal\web_service_client\TypedData\WsClientListDataDefinition",
 *   deriver = "Drupal\web_service_client\Plugin\Deriver\WsClientDataDeriver"
 * )
 */
class WsClientData extends Map {

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    /** @var \Drupal\Core\TypedData\DataDefinitionInterface $definition */
    parent::__construct($definition, $name, $parent);
    $cacheBackend = Drupal::service('cache.web_service_client');
    $cid = $name . '|data_type';
    $static_data = &drupal_static(__FUNCTION__ . ':' . $cid, NULL);
    if (!empty($name) && $static_data !== NULL) {
      $this->properties = $static_data;
    }
    else {
      if (!empty($name) && $cache = $cacheBackend->get($cid)) {
        $this->properties = $cache->data;
        $static_data = $this->properties;
      }
      else {
        // Initialize computed properties by default, such that they get cloned
        // with the whole item.
        $property_definitions = $this->definition->getPropertyDefinitions();
        if (is_array($property_definitions)) {
          foreach ($property_definitions as $key => $property_definition) {
            // Verify if the child property is of same type that parent and
            // if true skip this property because it will cause a infinite loop.
            if ($this->definition->getDataType() === $property_definition->getDataType()) {
              continue;
            }
            $this->properties[$key] = Drupal::typedDataManager()
              ->getPropertyInstance($this, $key);
          }
          if (!empty($name)) {
            $cacheBackend->set($cid, $this->properties);
          }
        }
        else {
          // Log non-array results, as they may be a bug in code.
          Drupal::logger('web_service_client')
            ->warning('Complex data-type %name returned no properties definitions.', [
              '%name' => $name ?? $definition->getLabel(),
            ]);
        }
      }
      $static_data = $this->properties;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    // Update the values and return them.
    foreach ($this->properties as $name => $property) {
      $value = $property->getValue();
      // Only write NULL values if the whole map is not NULL.
      if (isset($this->values) || isset($value)) {
        $this->values[$name] = $value;
      }
    }
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    $multiple = FALSE;
    if ($this->definition instanceof WsClientListDataDefinition) {
      $multiple = TRUE;
    }

    if (!$multiple && isset($values[0])) {
      $values = $values[0];
    }
    elseif ($multiple && !isset($values[0])) {
      $values = [$values];
    }

    if ($values !== NULL && !is_array($values)) {
      throw new InvalidArgumentException('Invalid values given. Values must be represented as an associative array.');
    }
    $this->values = $values;

    // Update any existing property objects.
    $memory_pull = [];
    foreach ($this->properties as $name => $property) {
      if (!isset($values[$name])) {
        continue;
      }
      $value = $values[$name] ?? NULL;
      if ($property instanceof ItemList && !is_array($value)) {
        $value = [$value];
      }
      $property->setValue($value, FALSE);
      // Remove the value from $this->values to ensure it does not contain any
      // value for computed properties.
      unset($this->values[$name]);
      $memory_pull[$name] = memory_get_usage(TRUE) / 1024 / 1024;
    }
    // Notify the parent of any changes.
    if ($notify && $this->parent !== NULL) {
      $this->parent->onChange($this->name);
    }
  }

  /**
   * Get child nodes using element trail.
   *
   * @param string $trail
   *   Element XPath trail.
   * @param mixed $response
   *   XML response.
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface|\Drupal\web_service_client\Plugin\DataType\WsClientData|mixed|null
   *   Child nodes.
   */
  public function getChildByTrail(string $trail, $response = NULL) {
    $trail = explode('/', $trail);
    array_shift($trail);
    $dataType = $this;
    foreach ($trail as $key => $child) {
      if ($dataType instanceof TypedData) {
        $dataType = $dataType->getChild($child, $dataType);
      }
      else {
        return NULL;
      }
    }
    if (!empty($response) && ($dataType instanceof self || $dataType instanceof WsClientItemList)) {
      $xsi_type = $response->getAttribute('type');
      if (!empty($xsi_type)) {
        list($namespace, $type) = explode(':', $xsi_type);
        if ($dataType instanceof ItemList) {
          $base_type = explode(':', $dataType->getItemDefinition()
            ->getDataType());
          $base_type[2] = $type;
          $type = implode(':', $base_type);
          $type_def = Drupal::typedDataManager()
            ->createListDataDefinition($type);
        }
        else {
          $base_type = explode(':', $dataType->definition->getDataType());
          $base_type[2] = $type;
          $type = implode(':', $base_type);
          $type_def = Drupal::typedDataManager()
            ->createDataDefinition($type);
        }
        $dataType = Drupal::typedDataManager()->create($type_def);
      }
      $dataType->rebuildChildrenTypesByResponse($response);
    }
    return $dataType;
  }

  /**
   * Get node child.
   *
   * @param string $child
   *   Child tag.
   * @param \Drupal\Core\TypedData\TypedData|null $type
   *   Current node type.
   *
   * @return mixed
   *   Child definition.
   */
  public function getChild(string $child, TypedData $type = NULL) {
    if (empty($type)) {
      return !empty($this->properties[$child]) ? $this->properties[$child] : NULL;
    }

    return !empty($type->properties[$child]) ? $type->properties[$child] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    if (!isset($this->properties[$property_name])) {
      $value = NULL;
      if (isset($this->values[$property_name])) {
        $value = $this->values[$property_name];
        return $value;
      }
      // If the property is unknown, this will throw an exception.
      $this->properties[$property_name] = $this->getTypedDataManager()
        ->getPropertyInstance($this, $property_name, $value);
    }
    return $this->properties[$property_name];
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return '';
  }

  /**
   * Rebuild children types.
   *
   * @param mixed $response
   *   Web service response.
   */
  public function rebuildChildrenTypesByResponse($response) {
    $children = $response->childNodes;
    $dataType = NULL;
    for ($i = 0; $i < $children->length; $i++) {
      $child = $children->item($i);
      if ($child->nodeType === XML_ELEMENT_NODE) {
        $xsi_type = $child->getAttribute('xsi:type');
        if (!empty($xsi_type)) {
          list($namespace, $type) = explode(':', $xsi_type);
          $base_type = explode(':', $this->definition->getDataType());
          $base_type[2] = $type;
          $type = implode(':', $base_type);
          if ($this instanceof ItemList) {
            $type_def = Drupal::typedDataManager()
              ->createListDataDefinition($type);
          }
          else {
            $type_def = Drupal::typedDataManager()
              ->createDataDefinition($type);
          }
          $dataType = Drupal::typedDataManager()->create($type_def);
        }

        /** @var self[]|\Drupal\web_service_client\Plugin\DataType\WsClientItemList $properties */
        $properties = &$this->properties;
        if (!empty($properties[$child->nodeName]) &&
          ($properties[$child->nodeName] instanceof self ||
            $properties[$child->nodeName] instanceof WsClientItemList)) {
          if ($dataType !== NULL) {
            $properties[$child->nodeName] = $dataType;
          }
          $properties[$child->nodeName]->rebuildChildrenTypesByResponse($child);
        }
      }
    }
  }

}
