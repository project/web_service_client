<?php

namespace Drupal\web_service_client;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining WS Client entities.
 */
interface WsClientInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
