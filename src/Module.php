<?php

namespace Drupal\web_service_client;

use Drupal;

/**
 * Module helper class.
 *
 * @package Drupal\web_service_client
 */
class Module {

  const MODULE_NAME = 'web_service_client';

  /**
   * Get the module instance.
   *
   * @return \Drupal\Core\Extension\Extension
   *   The module
   */
  public static function getHandler($module = FALSE) {
    $module = $module ? $module : self::MODULE_NAME;
    return Drupal::moduleHandler()
      ->getModule($module);
  }

}
