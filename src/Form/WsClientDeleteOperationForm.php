<?php

namespace Drupal\web_service_client\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete WS Client entities.
 */
class WsClientDeleteOperationForm extends WsClientDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $web_service_client_operation = NULL) {
    $form = parent::buildForm($form, $form_state);

    $form_state->set('web_service_client_operation', $web_service_client_operation);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operation = $form_state->get('web_service_client_operation');
    $this->entity->deleteOperation($operation);
    $this->entity->save();

    drupal_set_message(
      $this->t('content @type: deleted @label.',
        [
          '@type' => $this->entity->bundle(),
          '@label' => $this->entity->label(),
        ]
      )
    );
    $form_state->setRedirectUrl($this->entity->urlInfo('edit-form', ['web_service_client' => $this->entity->id()]));
  }

}
