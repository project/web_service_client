<?php

namespace Drupal\web_service_client\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WsClientDataForm.
 *
 * @package Drupal\web_service_client\Form
 */
class WsClientDataForm extends WsClientForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_service_client_data';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $web_service_client_data = NULL) {
    $service = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($web_service_client_data['label']) ? $web_service_client_data['label'] : '',
      '#required' => TRUE,
      '#description' => t('The human-readable name of the data type.'),
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($web_service_client_data['name']) ? $web_service_client_data['name'] : '',
      '#required' => TRUE,
      '#description' => t('The machine-readable name of this data type is used internally to identify the data type.'),
      '#element_validate' => [[$this, 'typeNameValidate']],
    ];
    $form['properties'] = [
      '#tree' => TRUE,
      '#element_validate' => [[$this, 'typeNameValidate']],
      '#theme' => 'web_service_client_property_form',
      '#title' => t('Properties'),
      '#description' => t('Specify the properties for the data type. For each property you have to specify a certain data type and a unique name containing only alphanumeric characters and underscores.'),
    ];
    $types = web_service_client_data_types();
    if (isset($web_service_client_data['property info'])) {
      foreach ($web_service_client_data['property info'] as $name => $info) {
        $form['properties']['items'][$name] = $this->propertyRow($service, $types, $name, $info);
      }
    }
    // Always add three empty lines.
    $mr = $form_state->get('more');
    $more = !empty($mr) ? $mr : 3;
    $form_state->set('more', $more);
    for ($i = 0; $i < $more; $i++) {
      if (!isset($form['properties']['items'][$i])) {
        $form['properties']['items'][$i] = $this->propertyRow($service, $types);
      }
    }
    $form['properties']['more'] = [
      '#type' => 'submit',
      '#value' => t('Add more'),
      '#limit_validation_errors' => [['properties']],
      '#submit' => [[$this, 'moreSubmit']],
    ];

    $form['field_settings'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => t('Field settings'),
      '#tree' => TRUE,
    ];

    $form['field_settings']['availability'] = [
      '#type' => 'checkbox',
      '#title' => t('Available as Field'),
      '#default_value' => isset($web_service_client_data['field_settings']['availability']) ? $web_service_client_data['field_settings']['availability'] : FALSE,
      '#description' => t('If check this TypedData will be available as entity fieldType, fieldWidget and fieldFormatter.'),
    ];

    $form['field_settings']['typed_data'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => t('Typed Data API'),
      '#tree' => TRUE,
    ];

    $form['field_settings']['typed_data']['definition_class'] = [
      '#type' => 'textfield',
      '#title' => t('Definition class'),
      '#default_value' => isset($web_service_client_data['field_settings']['typed_data']['definition_class']) ? $web_service_client_data['field_settings']['typed_data']['definition_class'] : FALSE,
    ];

    $form['field_settings']['typed_data']['list_definition_class'] = [
      '#type' => 'textfield',
      '#title' => t('List definition class'),
      '#default_value' => isset($web_service_client_data['field_settings']['typed_data']['list_definition_class']) ? $web_service_client_data['field_settings']['typed_data']['list_definition_class'] : FALSE,
    ];

    $form['field_settings']['typed_data']['class'] = [
      '#type' => 'textfield',
      '#title' => t('Plugin class'),
      '#default_value' => isset($web_service_client_data['field_settings']['typed_data']['class']) ? $web_service_client_data['field_settings']['typed_data']['class'] : FALSE,
    ];

    $form['field_settings']['typed_data']['list_class'] = [
      '#type' => 'textfield',
      '#title' => t('Plugin list class'),
      '#default_value' => isset($web_service_client_data['field_settings']['typed_data']['list_class']) ? $web_service_client_data['field_settings']['typed_data']['list_class'] : FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    $form_state->set('web_service_client_data', $web_service_client_data);

    // Allow the endpoint to make alterations to the form.
    $service->getEndpoint()->formDataAlter($form, $form_state);


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $service = $this->entity;
    $storage = $form_state->getStorage();
    $type = $storage['web_service_client_data'];
    $values = $form_state->getValues();
    $type['label'] = $values['label'];

    $type['property info'] = [];
    foreach ($values['properties']['items'] as $key => $item) {
      if (!empty($item['name'])) {
        $unmapped_type = $this->unmapType($item['type']);
        //$unmapped_type = $item['type'];
        $type['property info'][$item['name']] = [
          'type' => $unmapped_type,
          'label' => $item['label'],
          'multiple' => $item['multiple'],
        ];
      }
    }

    $type['field_settings'] = $form_state->getValue('field_settings');

    if (!empty($type['name'])) {
      $service->deleteDataType($type['name']);
    }
    $service->setDataType($values['name'], $type);
    $service->save();

    $service->getEndpoint()->formDataSubmit($form, $form_state);

    Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();

    drupal_set_message(t('Data type %type has been saved.', ['%type' => $type['label']]));

    $form_state->setRedirectUrl($service->urlInfo('edit-form', ['web_service_client' => $service->id()]));
  }

  /**
   * Generates a row in the properties table.
   */
  private function propertyRow($service, $types, $name = '', $info = []) {
    $property_type = 0;
    if (isset($info['type'])) {
      $property_type = $this->mapType($service->id(), $service->dataTypes(), $info['type']);
    }
    $property['type'] = [
      '#type' => 'select',
      '#options' => [0 => '--'] + $types,
      '#default_value' => $property_type,
    ];
    $property['multiple'] = [
      '#type' => 'checkbox',
      '#default_value' => isset($info['multiple']) ? $info['multiple'] : FALSE,
    ];
    $property['name'] = [
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $name,
      //'#element_validate' => array(array($this, 'typeNameValidate')),
    ];
    $property['label'] = [
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => isset($info['label']) ? $info['label'] : '',
    ];
    return $property;
  }

}
