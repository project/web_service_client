<?php

namespace Drupal\web_service_client\Form;

use Drupal;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\web_service_client\Entity\WsClient;

/**
 * Class WsClientForm.
 *
 * @package Drupal\web_service_client\Form
 */
class WsClientForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $service = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $service->label(),
      '#description' => $this->t("Label for the WS Client."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $service->id(),
      '#machine_name' => [
        'exists' => '\Drupal\web_service_client\Entity\WsClient::load',
      ],
      '#disabled' => !$service->isNew(),
    ];

    $types = Drupal::service('plugin.manager.web_service_client.type');
    $type_info = [];
    foreach ($types->getDefinitions() as $key => $value) {
      $type_info[$key] = $value['label'];
    }
    if (empty($type_info)) {
      drupal_set_message(t('No service types were found, please enable a module that provides a service type.'), 'warning');
    }
    $form['endpoint_url'] = [
      '#type' => 'textfield',
      '#title' => 'URL',
      '#default_value' => $service->getEndpointUrl(),
      '#required' => TRUE,
      '#description' => t('The URL of the web service.'),
      // '#element_validate' => array('wsclient_ui_element_url_validate').
    ];
    $form['type'] = [
      '#type' => 'select',
      '#title' => 'Type',
      '#default_value' => $service->getType(),
      '#options' => $type_info,
      '#required' => TRUE,
      '#description' => t('The type of the web service.'),
    ];
    if (!$service->isNew()) {
      // Operations of the web service in a table.
      $rows = [];
      $operations = $service->getOperations();
      $operations = !empty($operations) ? $this->labelSort($operations) : [];
      foreach ($operations as $name => $operation) {
        $row = [];

        $urlEditOp = Url::fromRoute('entity.web_service_client.edit_operation_form', [
          'web_service_client' => $service->id(),
          'web_service_client_operation' => $name,
        ]);
        $row[] = [
          'data' => [
            '#theme' => 'web_service_client_entity_ui_overview_item',
            '#label' => $operation['label'],
            '#name' => $name,
            '#url' => [
              'path' => $urlEditOp,
              'options' => [],
            ],
          ],
        ];
        $edit_link = Link::fromTextAndUrl(t('Edit'), $urlEditOp);
        $edit_link = $edit_link->toRenderable();
        $edit_link['#attributes'] = [
          'class' => [
            'button',
            'button--primary',
            'button--small',
          ],
        ];
        $row[] = render($edit_link);

        $urlDelOp = Url::fromRoute('entity.web_service_client.delete_operation_form', [
          'web_service_client' => $service->id(),
          'web_service_client_operation' => $name,
        ]);
        $delete_link = Link::fromTextAndUrl(t('Delete'), $urlDelOp);
        $delete_link = $delete_link->toRenderable();
        $delete_link['#attributes'] = [
          'class' => [
            'button',
            'button--primary',
            'button--small',
          ],
        ];
        $row[] = render($delete_link);

        $rows[] = $row;
      }
      $header = [
        t('Label'),
        [
          'data' => t('Operations'),
          'colspan' => 3,
        ],
      ];
      $urlAddOp = Url::fromRoute('entity.web_service_client.add_operation_form', ['web_service_client' => $service->id()]);
      $add_operation = [
        '#theme' => 'links__wsclient',
        '#links' => [
          'add_op' => [
            'title' => t('Add operation'),
            'url' => $urlAddOp,
            'attributes' => [
              'class' => [
                'button',
                'button-action',
                'button--primary',
                'button--small',
              ],
            ],
          ],
        ],
      ];
      $add_operation['#attributes']['class'][] = 'rules-operations-add';
      $add_operation['#attributes']['class'][] = 'action-links';
      $row = [];
      $row[] = ['data' => $add_operation, 'colspan' => 3];
      $rows[] = ['data' => $row, 'class' => ['rules-elements-add']];
      // @todo description help text for operations, data types
      $form['operations'] = [
        '#access' => TRUE,
        '#tree' => TRUE,
        '#type' => 'table',
        '#empty' => t('None'),
        '#caption' => t('Operations'),
        '#rows' => $rows,
        '#header' => $header,
      ];
      // Add some table styling from Rules.
      $form['operations']['#attributes']['class'][] = 'rules-elements-table';
      // $form['operations']['#attached']['css'][] = drupal_get_path('module', 'rules') . '/ui/rules.ui.css';

      // Data types of the web service in a table.
      $rows = [];
      $datatypes = $service->getDataTypes();
      $datatypes = !empty($datatypes) ? $this->labelSort($datatypes) : [];
      foreach ($datatypes as $name => $datatype) {
        $row = [];

        $urlEditData = Url::fromRoute('entity.web_service_client.edit_data_form', [
          'web_service_client' => $service->id(),
          'web_service_client_data' => $name,
        ]);
        $row[] = [
          'data' => [
            '#theme' => 'web_service_client_entity_ui_overview_item',
            '#label' => $datatype['label'],
            '#name' => $name,
            '#url' => [
              'path' => $urlEditData,
              'options' => [],
            ],
          ],
        ];
        $edit_link = Link::fromTextAndUrl(t('Edit'), $urlEditData);
        $edit_link = $edit_link->toRenderable();
        $edit_link['#attributes'] = [
          'class' => [
            'button',
            'button--primary',
            'button--small',
          ],
        ];
        $row[] = render($edit_link);

        $urlDelData = Url::fromRoute('entity.web_service_client.delete_data_form', [
          'web_service_client' => $service->id(),
          'web_service_client_data' => $name,
        ]);
        $delete_link = Link::fromTextAndUrl(t('Delete'), $urlDelData);
        $delete_link = $delete_link->toRenderable();
        $delete_link['#attributes'] = [
          'class' => [
            'button',
            'button--primary',
            'button--small',
          ],
        ];
        $row[] = render($delete_link);

        $rows[] = $row;
      }
      $header = [
        t('Label'),
        [
          'data' => t('Operations'),
          'colspan' => 3,
        ],
      ];
      $urlAddData = Url::fromRoute('entity.web_service_client.add_data_form', ['web_service_client' => $service->id()]);
      $add_type = [
        '#theme' => 'links__wsclient',
        '#links' => [
          'add_op' => [
            'title' => t('Add data type'),
            'url' => $urlAddData,
            'attributes' => [
              'class' => [
                'button',
                'button-action',
                'button--primary',
                'button--small',
              ],
            ],
          ],
        ],
      ];
      $add_type['#attributes']['class'][] = 'rules-operations-add';
      $add_type['#attributes']['class'][] = 'action-links';
      $row = [];
      $row[] = ['data' => $add_type, 'colspan' => 3];
      $rows[] = ['data' => $row, 'class' => ['rules-elements-add']];
      $form['datatypes'] = [
        '#access' => TRUE,
        '#tree' => TRUE,
        '#type' => 'table',
        '#empty' => t('None'),
        '#caption' => t('Data types'),
        '#rows' => $rows,
        '#header' => $header,
      ];

      // Input for global service parameters.
      $form['global_parameters'] = [
        '#tree' => TRUE,
        '#element_validate' => [[$this, 'validateGlobalParameters']],
        '#theme' => 'web_service_client_global_parameter_form',
        '#title' => t('Input for global service parameters'),
        '#description' => t('Specify the global parameters for the service. Global parameters will be used if the value of an operation parameter with the same name is empty.'),
      ];

      $weight = 0;
      foreach ($service->getGlobalParameters() as $name => $info) {
        $form['global_parameters']['items'][$name] = $this->globalParameterRow($service, $datatypes, $name, $info);
        $form['global_parameters']['items'][$name]['weight']['#default_value'] = $weight++;
      }

      // Always add three empty lines for global parameters input.
      $mr = $form_state->get('more');
      $more = !empty($mr) ? $mr : 3;
      $form_state->set('more', $more);
      for ($i = 0; $i < $more; $i++) {
        if (!isset($form['global_parameters']['items'][$i])) {
          $form['global_parameters']['items'][$i] = $this->globalParameterRow($service, $datatypes);
        }
      }
      $form['global_parameters']['more'] = [
        '#type' => 'submit',
        '#value' => t('Add more'),
        '#limit_validation_errors' => [['properties']],
        '#submit' => [[$this, 'moreSubmit']],
      ];

      // Add some table styling from Rules.
      $form['datatypes']['#attributes']['class'][] = 'rules-elements-table';
      //$form['datatypes']['#attached']['css'][] = drupal_get_path('module', 'rules') . '/ui/rules.ui.css';
    }

    // Allow the endpoint to make alterations to the form.
    if ($service->getType()) {
      $service->getEndpoint()->formAlter($form, $form_state);
    }

    return $form;
  }

  public function updateDataTypes(array $form, FormStateInterface $form_state) {
    $service = WsClient::load($this->entity->id());
    $end_point = $service->getEndpoint();
    $end_point->init($service);
    $end_point->updateDataTypes();
  }


  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['update_data_types'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update data types'),
      '#submit' => ['::updateDataTypes'],
    ];

    return $actions;
  }

  /**
   * Helper function to sort a nested data information array based on the label
   * of the items.
   */
  public function labelSort($data_info, $label_key = 'label') {
    $sort_info = [];
    foreach ($data_info as $key => $info) {
      $sort_info[$key] = $info[$label_key];
    }
    natcasesort($sort_info);
    foreach ($sort_info as $key => $label) {
      $sort_info[$key] = $data_info[$key];
    }
    return $sort_info;
  }

  /**
   * Generates a row in the global parameter table.
   */
  public function globalParameterRow($service, $types, $name = '', $info = []) {
    $param_type = 0;
    $multiple = FALSE;
    $parameter['name'] = [
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $name,
      '#element_validate' => [[$this, 'typeNameValidate']],
    ];
    $parameter['default_value'] = [
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => isset($info['default value']) ? $info['default value'] : '',
    ];
    return $parameter;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $service = WsClient::load($this->entity->id());
    if (empty($service)) {
      $service = $this->entity;
    }

    // Save global paramters.
    $global_parameters = $form_state->getValue('global_parameters');
    $glob = [];
    if (!empty($global_parameters['items'])) {
      foreach ($global_parameters['items'] as $key => $item) {
        if (!empty($item['name'])) {
          $glob[$item['name']] = [
            'default value' => $item['default_value'],
          ];
        }
      }
    }
    $service->setGlobalParameters($glob);

    $url = $form_state->getValue('endpoint_url');
    $service->setEndpointUrl($url);

    $status = $service->save();

    $service->getEndpoint()->formSubmit($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label WS Client.', [
          '%label' => $service->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label WS Client.', [
          '%label' => $service->label(),
        ]));
    }
    $form_state->setRedirectUrl($service->urlInfo('collection'));
  }

  /**
   * Submit callback for adding more parameter rows.
   */
  public function moreSubmit($form, &$form_state) {
    $num_rows = $form_state->get('more');
    $num_rows++;
    $form_state->set('more', $num_rows);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Maps the type name from the name used by the remote info to the rules name.
   */
  public function mapType($service_name, $service_types, $type) {
    if (is_array($type)) {
      foreach ($type as $key => $entry) {
        $type[$key] = $this->mapType($service_name, $service_types, $entry);
      }
      return $type;
    }
    if (isset($service_types[$type])) {
      return 'web_service_client:' . $service_name . ':' . $type;
    }
    if (strpos($type, 'list<') === 0 && isset($service_types[substr($type, 5, -1)])) {
      return 'list<wsclient_' . $service_name . '_' . substr($type, 5, -1) . '>';
    }
    return $type;
  }

  /**
   * FAPI callback to validate the form for editing parameter info.
   */
  public function validateParameters($elements, FormStateInterface $form_state) {
    $names = [];
    foreach (Element::children($elements['items']) as $item_key) {
      $element = &$elements['items'][$item_key];
      if ($element['name']['#value'] || $element['type']['#value']) {
        foreach ([
                   'name' => t('Name'),
                   'type' => t('Data type'),
                 ] as $key => $title) {
          if (!$element[$key]['#value']) {
            $form_state->setErrorByName($element[$key]['#name'], t('!name field is required.', ['!name' => $title]));
          }
        }
        if (isset($names[$element['name']['#value']])) {
          $form_state->setErrorByName($element['name']['#name'], t('The name %name is already taken.', ['%name' => $element['name']['#value']]));
        }
        $names[$element['name']['#value']] = TRUE;
      }
      if ($element['type']['#value'] == 'hidden' && $element['default_value']['#value'] === '') {
        $form_state->setErrorByName($element['default_value'], t('The "hidden" data type requires a default value.'));
      }
    }
  }

  /**
   * Validation callback for data type names.
   */
  public function typeNameValidate($element, FormStateInterface $form_state) {
    if (!empty($element['#value']) && !preg_match('!^[A-Za-z0-9_]+$!', $element['#value'])) {
      $form_state->setErrorByName($element['#name'], t('Data type names must contain only letters, numbers, and underscores.'));
    }
    $service = $this->entity;
    $storage = $form_state->getStorage();
    $type = !empty($storage['web_service_client_data']) ? $storage['web_service_client_data'] : [];
    $dataTypes = $service->getDataTypes();
    if (!empty($element['#value']) && !empty($type['name']) && $element['#value'] != $type['name'] && isset($dataTypes) && in_array($element['#value'], array_keys($dataTypes))) {
      $form_state->setErrorByName($element['#name'], t('A data type with that name already exists'));
    }
  }

  /**
   * FAPI callback to validate the form for editing global parameter info.
   */
  public function validateGlobalParameters($elements, &$form_state) {
    $names = [];
    foreach (Element::children($elements['items']) as $item_key) {
      $element = &$elements['items'][$item_key];
      if (!empty($element['name']['#value'])) {
        if (isset($names[$element['name']['#value']])) {
          $form_state->setErrorByName($elements['items'][$item_key], t('The name %name is already taken.', ['%name' => $element['name']['#value']]));
        }
        $names[$element['name']['#value']] = TRUE;
      }
    }
  }

  public function unmapType($item) {
    $exp = explode(':', $item);
    if (!empty($exp[2])) {
      return $exp[2];
    }
    return $item;
  }

}

