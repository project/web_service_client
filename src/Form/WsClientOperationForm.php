<?php

namespace Drupal\web_service_client\Form;

use Drupal;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WsClientOperationForm.
 *
 * @package Drupal\web_service_client\Form
 */
class WsClientOperationForm extends WsClientForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_service_client_operation';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $web_service_client_operation = NULL) {
    $service = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => isset($web_service_client_operation['label']) ? $web_service_client_operation['label'] : '',
      '#required' => TRUE,
      '#description' => t('The human-readable name of the operation.'),
      '#weight' => -10,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($web_service_client_operation['name']) ? $web_service_client_operation['name'] : '',
      '#required' => TRUE,
      '#description' => t('The machine-readable name of this operation is used internally to identify the operation.'),
      '#element_validate' => [[$this, 'operationNameValidate']],
      '#weight' => -10,
    ];
    $form['parameters'] = [
      '#tree' => TRUE,
      '#element_validate' => [[$this, 'validateParameters']],
      '#theme' => 'web_service_client_parameter_form',
      '#title' => t('Parameters'),
      '#description' => t('Specify the parameters for the operation. For each parameter you have to specify a certain data type and a unique name containing only alphanumeric characters and underscores. You can also specify a default value for the parameter and if it is required.'),
    ];
    $weight = 0;
    $types = web_service_client_data_types(TRUE);
    if (isset($web_service_client_operation['parameter'])) {
      foreach ($web_service_client_operation['parameter'] as $name => $info) {
        $form['parameters']['items'][$name] = $this->parameterRow($service, $types, $name, $info);
        $form['parameters']['items'][$name]['weight']['#default_value'] = $weight++;
      }
    }
    // Always add three empty lines.
    $mr = $form_state->get('more');
    $more = !empty($mr) ? $mr : 3;
    $form_state->set('more', $more);
    for ($i = 0; $i < $more; $i++) {
      if (!isset($form['parameters']['items'][$i])) {
        $form['parameters']['items'][$i] = $this->parameterRow($service, $types);
        $form['parameters']['items'][$i]['weight']['#default_value'] = $weight++;
      }
    }
    $form['parameters']['more'] = [
      '#type' => 'submit',
      '#value' => t('Add more'),
      '#limit_validation_errors' => [['parameters']],
      '#submit' => [[$this, 'moreSubmit']],
    ];
    // Exclude the hidden data type for result types.
    unset($types['hidden']);

    $result_type = 0;
    if (isset($web_service_client_operation['result']['type'])) {
      $result_type = $this->mapType($service->id(), $service->dataTypes(), $web_service_client_operation['result']['type']);
    }
    $form['result_type'] = [
      '#type' => 'select',
      '#title' => t('Result type'),
      '#options' => [0 => '--'] + $types,
      '#default_value' => $result_type,
      '#description' => t('The result data type returned by the service'),
    ];
    $form['result_multiple'] = [
      '#type' => 'checkbox',
      '#title' => t('Multiple result'),
      '#default_value' => isset($web_service_client_operation['result']['multiple']) ? $web_service_client_operation['result']['multiple'] : FALSE,
      '#description' => t('If checked, the result variable is a list containing multiple elements of the result type.'),
    ];
    $form['result_label'] = [
      '#type' => 'textfield',
      '#title' => t('Result label'),
      '#default_value' => isset($web_service_client_operation['result']['label']) ? $web_service_client_operation['result']['label'] : '',
      '#description' => t('The human-readable name of the result variable returned by the service.'),
    ];

    $form['views_settings'] = [
      '#type' => 'details',
      '#open' => FALSE,
      '#title' => t('Views settings'),
      '#tree' => TRUE,
    ];

    $form['views_settings']['availability'] = [
      '#type' => 'checkbox',
      '#title' => t('Available as View'),
      '#default_value' => isset($web_service_client_operation['views_settings']['availability']) ? $web_service_client_operation['views_settings']['availability'] : FALSE,
      '#description' => t('If check the result of this operation will be available as view.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    $form_state->set('web_service_client_operation', $web_service_client_operation);

    // Allow the endpoint to make alterations to the form.
    $service->getEndpoint()->formOperationAlter($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $service = $this->entity;
    $storage = $form_state->getStorage();
    $operation = $storage['web_service_client_operation'];

    $values = $form_state->getValues();
    $operation['label'] = $values['label'];
    $operation['parameter'] = [];
    foreach ($values['parameters']['items'] as $key => $item) {
      if (!empty($item['name'])) {
        // Unmap the data type if it is local to this service.
        $unmapped_type = $this->unmapType($item['type']);
        //$unmapped_type = $item['type'];
        $operation['parameter'][$item['name']] = ['type' => $unmapped_type];
        if ($item['default_value'] !== '') {
          $operation['parameter'][$item['name']]['default value'] = $item['default_value'];
        }
        $operation['parameter'][$item['name']]['multiple'] = $item['multiple'];
        if ($item['required']) {
          $operation['parameter'][$item['name']]['optional'] = TRUE;
        }
      }
    }
    if (!empty($values['result_type'])) {
      $unmapped_type = $this->unmapType($form_state->getValue('result_type'));
      //$unmapped_type = $form_state->getValue('result_type');
      $operation['result'] = [
        'type' => $unmapped_type,
        'label' => isset($values['result_label']) ? $values['result_label'] : 'result',
        'multiple' => $values['result_multiple'],
      ];
    }

    $operation['views_settings'] = $form_state->getValue('views_settings');

    if (!empty($operation['name'])) {
      $service->deleteOperation($operation['name']);
    }
    $service->setOperation($values['name'], $operation);

    $service->save();

    $service->getEndpoint()->formOperationSubmit($form, $form_state);

    Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();

    drupal_set_message(t('Operation %operation has been saved.', ['%operation' => $operation['label']]));

    $form_state->setRedirectUrl($service->urlInfo('edit-form', ['web_service_client' => $service->id()]));
  }

  /**
   * Generates a row in the parameter table.
   */
  public function parameterRow($service, $types, $name = '', $info = []) {
    $param_type = 0;
    if (isset($info['type'])) {
      $param_type = $this->mapType($service->id(), $service->dataTypes(), $info['type']);
    }
    $parameter['type'] = [
      '#type' => 'select',
      '#options' => [0 => '--'] + $types,
      '#default_value' => $param_type,
    ];
    $parameter['multiple'] = [
      '#type' => 'checkbox',
      '#default_value' => isset($info['multiple']) ? $info['multiple'] : FALSE,
    ];
    $parameter['name'] = [
      '#type' => 'textfield',
      '#size' => 40,
      '#default_value' => $name,
      '#element_validate' => [[$this, 'typeNameValidate']],
    ];
    $parameter['default_value'] = [
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => isset($info['default value']) ? $info['default value'] : '',
    ];
    $parameter['required'] = [
      '#type' => 'checkbox',
      '#default_value' => isset($info['optional']) ? $info['optional'] : FALSE,
    ];
    $parameter['weight'] = [
      '#type' => 'weight',
    ];
    return $parameter;
  }

  /**
   * Validation callback for operation names.
   */
  public function operationNameValidate($element, FormStateInterface $form_state) {
    if (!empty($element['#value']) && !preg_match('!^[A-Za-z0-9_]+$!', $element['#value'])) {
      $form_state->setErrorByName($element['#name'], t('Operation names must contain only letters, numbers, and underscores.'));
    }
    $service = $this->entity;
    $storage = $form_state->getStorage();
    $op = $storage['web_service_client_operation'];
    $operations = $service->getOperations();
    if (!empty($element['#value']) && $element['#value'] != $op['name'] && isset($operations[$element['#value']])) {
      $form_state->setErrorByName($element['#name'], t('An operation with that name already exists'));
    }
  }

}
