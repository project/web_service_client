<?php

namespace Drupal\web_service_client\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\web_service_client\Entity\WsClient;
use Symfony\Component\Routing\Route;

class OperationParamConverter implements ParamConverterInterface {

  public function convert($value, $definition, $name, array $defaults) {
    $service = $defaults['web_service_client'];
    if ($service instanceof WsClient) {
      $operations = $service->getOperations();
      if (isset($operations[$value])) {
        $operation = $operations[$value];
        $operation['name'] = $value;
        return $operation;
      }
    }
    return FALSE;
  }

  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'web_service_client_operation');
  }

}
