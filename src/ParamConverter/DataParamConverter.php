<?php

namespace Drupal\web_service_client\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\web_service_client\Entity\WsClient;
use Symfony\Component\Routing\Route;

class DataParamConverter implements ParamConverterInterface {

  public function convert($value, $definition, $name, array $defaults) {
    $service = $defaults['web_service_client'];
    if ($service instanceof WsClient) {
      $data_types = $service->getDataTypes();
      if (isset($data_types[$value])) {
        $data_type = $data_types[$value];
        $data_type['name'] = $value;
        return $data_type;
      }
    }
    return FALSE;
  }

  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] == 'web_service_client_data');
  }

}
