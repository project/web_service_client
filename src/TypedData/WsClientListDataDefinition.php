<?php

namespace Drupal\web_service_client\TypedData;

use Drupal\Core\TypedData\ListDataDefinition;

/**
 * A typed data definition class for defining WsClientData lists.
 */
class WsClientListDataDefinition extends ListDataDefinition {

  /**
   * {@inheritdoc}
   */
  public function getDataType() {
    return 'web_service_client_list';
  }

}
