<?php

namespace Drupal\web_service_client\TypedData;

use Drupal;
use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\web_service_client\Entity\WsClient;


class WsClientDataDefinition extends ComplexDataDefinitionBase {

  protected $dataType;

  protected $wscDataType;

  /**
   * An array of form data definitions.
   *
   * @var \Drupal\Core\TypedData\DataDefinitionInterface[]
   */
  protected $arrayFormDefinition;

  public function getPropertyDefinitions() {
    $cacheBackend = Drupal::service('cache.web_service_client');
    $cid = $this->definition['type'] . '|data_property_definition';
    $static_data = &drupal_static(__FUNCTION__ . ':' . $cid, NULL);
    if ($static_data !== NULL) {
      $this->propertyDefinitions = $static_data;
      return $this->propertyDefinitions;
    }
    if ($cache = $cacheBackend->get($cid)) {
      $this->propertyDefinitions = $cache->data;
      $static_data = $this->propertyDefinitions;
      return $this->propertyDefinitions;
    }

    list($provider, $service, $dataType) = explode(':', $this->definition['type']);
    if ($provider == 'web_service_client' && !empty($service)) {
      $service = WsClient::load($service);
      if (!empty($service) && $service instanceof WsClient) {
        $dataTypes = $service->getDataTypes();
        if (!empty($dataTypes[$dataType])) {
          $this->dataType = $dataTypes[$dataType];
        }
      }
    }

    if (!isset($this->propertyDefinitions) && !empty($this->dataType)) {
      $types = web_service_client_data_types();
      $types = $types['Others'];
      foreach ($this->dataType['property info'] as $key => $property) {
        $type = $property['type'];
        if (empty($types[$type])) {
          $this->wscDataType = $property;
          $type = $provider . ':' . $service->id() . ':' . $property['type'];
          $label = !empty($property['label']) ? $property['label'] : $key;

          if (!empty($property['multiple'])) {
            $this->propertyDefinitions[$key] = WsClientListDataDefinition::create($type)
              ->setLabel($label);
          }
          else {
            $this->propertyDefinitions[$key] = WsClientDataDefinition::create($type)
              ->setLabel($label);
          }
        }
        else {

          if (!empty($property['multiple'])) {
            $this->propertyDefinitions[$key] = ListDataDefinition::create($type)
              ->setLabel($key);
          }
          else {
            $this->propertyDefinitions[$key] = DataDefinition::create($type)
              ->setLabel($key);
          }
        }
      }
    }

    $static_data = $this->propertyDefinitions;
    $cacheBackend->set($cid, $this->propertyDefinitions);
    return $this->propertyDefinitions;
  }

  public function getWscDataType() {
    return $this->wscDataType;
  }

  public function setPropertyDefinitions($properties) {
    $this->propertyDefinitions = $properties;
  }

}
