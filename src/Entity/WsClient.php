<?php

namespace Drupal\web_service_client\Entity;

use Drupal;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Driver\Exception\Exception;
use Drupal\web_service_client\WsClientInterface;

/**
 * Defines the WS Client entity.
 *
 * @ConfigEntityType(
 *   id = "web_service_client",
 *   label = @Translation("WS Client"),
 *   handlers = {
 *     "list_builder" = "Drupal\web_service_client\WsClientListBuilder",
 *     "form" = {
 *       "add" = "Drupal\web_service_client\Form\WsClientForm",
 *       "edit" = "Drupal\web_service_client\Form\WsClientForm",
 *       "delete" = "Drupal\web_service_client\Form\WsClientDeleteForm",
 *       "add_operation" =
 *   "Drupal\web_service_client\Form\WsClientOperationForm",
 *       "edit_operation" =
 *   "Drupal\web_service_client\Form\WsClientOperationForm",
 *       "delete_operation" =
 *   "Drupal\web_service_client\Form\WsClientDeleteOperationForm",
 *       "add_data" = "Drupal\web_service_client\Form\WsClientDataForm",
 *       "edit_data" = "Drupal\web_service_client\Form\WsClientDataForm",
 *       "delete_data" =
 *   "Drupal\web_service_client\Form\WsClientDeleteDataForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\web_service_client\WsClientHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "web_service_client",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "endpoint_url",
 *     "operations",
 *     "datatypes",
 *     "enumerations",
 *     "global_parameters",
 *     "settings",
 *     "authentication",
 *     "type"
 *   },
 *   links = {
 *     "canonical" =
 *   "/admin/config/services/web_service_client/{web_service_client}",
 *     "add-form" = "/admin/config/services/web_service_client/add",
 *     "edit-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/edit",
 *     "delete-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/delete",
 *     "add-operation-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/add/operation",
 *     "edit-operation-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/operation/{web_service_client_operation}/edit",
 *     "delete-operation-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/operation/{web_service_client_operation}/delete",
 *     "add-data-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/add/data",
 *     "edit-data-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/data/{web_service_client_data}/edit",
 *     "delete-data-form" =
 *   "/admin/config/services/web_service_client/{web_service_client}/data/{web_service_client_data}/delete",
 *     "collection" = "/admin/config/services/web_service_client"
 *   }
 * )
 */
class WsClient extends ConfigEntityBase implements WsClientInterface {

  /**
   * The WS Client ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The WS Client label.
   *
   * @var string
   */
  protected $label;

  /**
   * The url of the web service.
   *
   * @var string
   */
  protected $endpoint_url;

  /**
   * The operations this web service offers.
   *
   * @var array
   */
  protected $operations;

  /**
   * The data types this web service offers.
   *
   * @var array
   */
  protected $datatypes;

  /**
   * The enumerations this web service offers.
   *
   * @var array
   */
  protected $enumerations;

  /**
   * The global parameters definition of this web service..
   *
   * @var array
   */
  protected $global_parameters;

  /**
   * The type of the remote endpoint.
   *
   * @var string
   */
  protected $type;

  /**
   * The endpoint type specific settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * Data describing the authentication method.
   *
   * @var array
   */
  protected $authentication;

  /**
   * The endpoint.
   *
   * @var WsClientTypePluginInterface
   */
  protected $endpoint;


  public function getEndpointUrl() {
    return $this->endpoint_url;
  }

  public function setEndpointUrl($url) {
    $this->endpoint_url = $url;
  }

  public function getType() {
    return $this->type;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function getOperations() {
    return $this->operations;
  }

  public function setOperations($operations) {
    $this->operations = $operations;
  }

  public function setOperation($name, $operation) {
    $this->operations[$name] = $operation;
  }

  public function deleteOperation($name) {
    if (!empty($this->operations[$name])) {
      unset($this->operations[$name]);
      return TRUE;
    }
    return FALSE;
  }

  public function getDataTypes() {
    return $this->datatypes;
  }

  public function setDataTypes($dataTypes) {
    $this->datatypes = $dataTypes;
  }

  public function setDataType($name, $dataTypes) {
    $this->datatypes[$name] = $dataTypes;
  }

  public function deleteDataType($name) {
    if (!empty($this->datatypes[$name])) {
      unset($this->datatypes[$name]);
      return TRUE;
    }
    return FALSE;
  }

  public function getEnumerations() {
    return $this->enumerations;
  }

  public function setEnumerations($enumerations) {
    $this->enumerations = $enumerations;
  }

  public function getGlobalParameters() {
    return $this->global_parameters;
  }

  public function setGlobalParameters($global_parameters) {
    $this->global_parameters = $global_parameters;
  }

  public function getSettings() {
    return $this->settings;
  }

  public function setSettings($settings) {
    $this->settings = $settings;
  }

  public function getAuthentication() {
    return $this->authentication;
  }

  public function setAuthentication($authentication) {
    $this->authentication = $authentication;
  }


  /**
   * Magic method to catch service invocations.
   */
  public function __call($operation, $arguments) {
    return $this->invoke($operation, $arguments);
  }

  /**
   * Invoke a service via its endpoint.
   *
   * @param string $operation
   *   The name of the operation to execute.
   *
   * @param array $arguments
   *   Arguments to pass to the service with this operation. If the array keys
   *   do not match the parameter information, the array values are assigned in
   *   sequential order according to the order of parameters.
   */
  public function invoke($operation, array $arguments) {
    if (!isset($this->operations[$operation])) {
      $msg = t('Operation %operation does not exist for web service %service.', [
        '%operation' => $operation,
        '%service' => $this->id(),
      ]);
      throw new \Exception($msg);
    }
    $named_arguments = [];
    if (isset($this->operations[$operation]['parameter'])) {
      $remaining_params = $this->operations[$operation]['parameter'];
      // Assign named arguments and hidden parameters.
      foreach ($this->operations[$operation]['parameter'] as $param => $info) {
        if (isset($arguments[$param])) {
          $named_arguments[$param] = $arguments[$param];
          unset($arguments[$param]);
          unset($remaining_params[$param]);
        }
        elseif ($info['type'] == 'hidden') {
          $named_arguments[$param] = $info['default value'];
          unset($remaining_params[$param]);
        }
      }
      // Assign the rest in sequential order.
      foreach ($remaining_params as $param => $info) {
        $named_arguments[$param] = array_shift($arguments);
      }
    }

    // Fill in global parameters.
    foreach ($this->global_parameters as $param => $info) {
      if (!isset($named_arguments[$param])) {
        $named_arguments[$param] = $info['default value'];
      }
    }

    return $this->endpoint()->call($operation, $named_arguments);
  }

  /**
   * Determines access to the web service.
   */
  //    public function access($account = NULL) {
  //        if (method_exists($this->endpoint(), 'access')) {
  //            return $this->endpoint()->access($account);
  //        }
  //        return $account->hasPermission('interact with ' . $this->id, $account);
  //    }

  /**
   * Returns the associated web service endpoint object.
   *
   * @return WsClientTypePluginInterface
   */
  public function getEndpoint() {
    if (!isset($this->endpoint)) {
      $types = Drupal::service('plugin.manager.web_service_client.type');
      $this->endpoint = $types->createInstance($this->type);
      $this->endpoint->init($this);
    }
    return $this->endpoint;
  }

  /**
   * Returns info about the data types of the web service.
   */
  public function dataTypes() {
    if (empty($this->datatypes)) {
      return [];
    }
    $types = $this->datatypes;
    foreach ($types as $type => $info) {
      // Add in the service name so that others know where this type comes from.
      $types[$type]['service'] = $this->id();
    }
    return $types;
  }

  /**
   * Returns info about the actions of the web service.
   */
  public function actions() {
    $actions = [];
    foreach ($this->operations as $name => $operation) {
      $actions[$name] = $operation += [
        'base' => 'wsclient_service_action',
        'named parameter' => TRUE,
      ];
      $actions[$name]['parameter'] = [];
      if (isset($operation['parameter'])) {
        // Prefix operation parameter names to avoid name clashes.
        foreach ((array) $operation['parameter'] as $param => $info) {
          $actions[$name]['parameter']['param_' . $param] = $info;
        }
      }
      $actions[$name]['parameter']['service'] = [
        'type' => 'hidden',
        'default value' => $this->id(),
      ];
      $actions[$name]['parameter']['operation'] = [
        'type' => 'hidden',
        'default value' => $name,
      ];
      // Pass through the service result as provided variable.
      if (!empty($actions[$name]['result'])) {
        $actions[$name]['provides']['result'] = $actions[$name]['result'];
        unset($actions[$name]['result']);
      }
    }
    return $actions;
  }

  public function clearCache() {
    $this->endpoint()->clearCache();
  }

}
