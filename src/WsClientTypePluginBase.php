<?php

namespace Drupal\web_service_client;

use DOMDocument;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\web_service_client\Entity\WsClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class WsClientTypePluginBase extends PluginBase implements WsClientTypePluginInterface {

  protected $service;

  protected $url;

  protected $client;

  /**
   * Constructs a ReusableFormPluginBase object.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  public function init(WsClient $service) {
    $this->service = $service;
    $this->url = $service->getEndpointUrl();
  }

  public function call($operation, array $arguments) {
  }

  public function dataTypes() {
  }

  public function formAlter(&$form, FormStateInterface $form_state) {
  }

  public function formSubmit(&$form, FormStateInterface $form_state) {
  }

  public function formOperationAlter(&$form, FormStateInterface $form_state) {
  }

  public function formOperationSubmit(&$form, FormStateInterface $form_state) {
  }

  public function formDataAlter(&$form, FormStateInterface $form_state) {
  }

  public function formDataSubmit(&$form, FormStateInterface $form_state) {
  }

  public function clearCache() {
    unset($this->client);
  }

  public function getDefinitionExtraRelationship($table_key, $data_type) {
    return [];
  }

  public function getDefinitionExtraBase($op_name) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function dataProcessOperationsAlter(&$properties, $result_type, $operations, $data_types, $primitive_types) {

  }

  /**
   * Build XSL document.
   *
   * @param mixed $service
   *   The web service.
   * @param string $table_key
   *   The views table key.
   * @param string $prop_key
   *   The property key.
   * @param mixed $properties
   *   Properties.
   *
   * @return \DOMDocument
   *   The XSL document.
   */
  protected function buildXslDocument($service, $table_key, $prop_key, $properties) {
    $xslDoc = new DOMDocument('1.0', 'UTF-8');
    $xslDoc->preserveWhiteSpace = FALSE;
    $xslDoc->formatOutput = TRUE;

    $xslRoot = $xslDoc->createElement("xsl:stylesheet");
    $xslRoot->setAttribute("version", "1.0");
    $xslRoot->setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");

    $match = '';
    $xslTemplate = $xslDoc->createElement("xsl:template");
    $xslTemplate->setAttribute("match", $match);

    $results = $xslDoc->createElement('results');

    $select = '';
    $xslApplyTemplate = $xslDoc->createElement("xsl:apply-templates");
    $xslApplyTemplate->setAttribute("select", $select);

    $results->appendChild($xslApplyTemplate);
    $xslTemplate->appendChild($results);
    $xslRoot->appendChild($xslTemplate);

    $relationshipMatch = '';
    $xslRelationshipTemplate = $xslDoc->createElement("xsl:template");
    $xslRelationshipTemplate->setAttribute('match', $relationshipMatch);

    $result = $xslDoc->createElement("result");

    foreach (['x', 'y'] as $value) {
      $xslCopySelect = $value;
      $xslCopy = $xslDoc->createElement("xsl:copy-of");
      $xslCopy->setAttribute("select", $xslCopySelect);
      $result->appendChild($xslCopy);
    }

    $xslRelationshipTemplate->appendChild($result);
    $xslRoot->appendChild($xslRelationshipTemplate);

    $xslDoc->appendChild($xslRoot);

    return $xslDoc;
  }

}
