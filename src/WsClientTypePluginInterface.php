<?php

namespace Drupal\web_service_client;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

interface WsClientTypePluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Connect to the remote web service and call an operation.
   *
   * @param string $operation
   *   The name of the operation.
   * @param array $arguments
   *   The arguments needed for the operation.
   *
   * @return
   *   The response of the web service call.
   */
  public function call($operation, array $arguments);

  /**
   * An array of info about data types used by the provided events/actions
   * being not entities.
   */
  public function dataTypes();

  /**
   * Allows altering the configuration form of web service definitions, such
   * that the form can include endpoint type specific configuration settings.
   */
  public function formAlter(&$form, FormStateInterface $form_state);

  public function formSubmit(&$form, FormStateInterface $form_state);

  public function formOperationAlter(&$form, FormStateInterface $form_state);

  public function formOperationSubmit(&$form, FormStateInterface $form_state);

  public function formDataAlter(&$form, FormStateInterface $form_state);

  public function formDataSubmit(&$form, FormStateInterface $form_state);

  /**
   * Clear any caches the endpoint maintains.
   */
  public function clearCache();

  public function getDefinitionExtraRelationship($table_key, $data_type);

  public function getDefinitionExtraBase($op_name);

  /**
   * Modify the properties of the result of the operation performed.
   *
   * @param $properties
   *   The properties to modify.
   * @param $result_type
   *   The type of the result
   * @param $operations
   *   The operations of the result type.
   * @param $data_types
   *   The data types of the endpoint.
   * @param $primitive_types
   *   The primitives types of the endpoint.
   */
  public function dataProcessOperationsAlter(&$properties, $result_type, $operations, $data_types, $primitive_types);

}
