<?php

namespace Drupal\web_service_client;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

class WsClientTypePluginManager extends DefaultPluginManager {

  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/WsClient/WsType',
      $namespaces, $module_handler,
      'Drupal\web_service_client\WsClientTypePluginInterface', 'Drupal\web_service_client\Annotation\WsClientType');
    $this->alterInfo('web_service_client_type_info');
    $this->setCacheBackend($cache_backend, 'web_service_client_type');
  }


  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    // Add the module or theme path to the 'path'.
    if ($this->moduleHandler->moduleExists($definition['provider'])) {
      $definition['provider_type'] = 'module';
      $definition['definition_path'] = $this->moduleHandler->getModule($definition['provider'])
        ->getPath();
    }
    else {
      $definition['definition_path'] = '';
    }
  }

}
