<?php

namespace Drupal\web_service_client_soap;

use Drupal\views\ResultRow;

/**
 * A class representing a view result row.
 */
class SoapResultRow extends ResultRow {

  public $raw;

  protected $baseTable;

  protected $service;

  public function __construct($base_table, $xml, $service_id, array $values = [], array $data = NULL) {
    if (!empty($data)) {
      $this->setProcessedData($data);
    }
    else {
      $this->baseTable = $base_table['result_type'];
      $this->service = $service_id;
      $row = self::domToArray($xml);
      $this->raw = $row['row'];
    }

    parent::__construct($values);
  }

  public static function domToArray($root) {
    $array = [];
    // Handle classic node.
    if ($root->nodeType === XML_ELEMENT_NODE) {
      $array[$root->nodeName] = [];
      if ($root->hasChildNodes()) {
        $children = $root->childNodes;
        for ($i = 0; $i < $children->length; $i++) {
          $child = self::domToArray($children->item($i));
          // Don't keep textnode with only spaces and newline.
          if (is_array($child)) {
            reset($child);
            $first_key = key($child);
            if (is_array($child[$first_key]) && count($child[$first_key]) > 1 && !isset($child[$first_key][0])) {
              $array[$root->nodeName][$first_key][] = $child[$first_key];
            }
            elseif (isset($array[$root->nodeName][$first_key])) {
              if (!is_array($array[$root->nodeName][$first_key])) {
                $array[$root->nodeName][$first_key] = [$array[$root->nodeName][$first_key]];
              }
              $array[$root->nodeName][$first_key][] = $child[$first_key];
            }
            else {
              $array[$root->nodeName][$first_key] = $child[$first_key];
            }
          }
          elseif (!empty($child) || $child == 0) {
            $array[$root->nodeName] = $child;
          }
        }
      }
      // Handle text node.
    }
    elseif ($root->nodeType === XML_TEXT_NODE || $root->nodeType === XML_CDATA_SECTION_NODE) {
      $value = $root->nodeValue;
      if (!empty($value) || $value == 0) {
        $array = $value;
      }
    }
    return $array;
  }

  public function getRaw() {
    if (!empty($this->raw)) {
      return $this->raw;
    }
    return NULL;
  }

}
