<?php

namespace Drupal\web_service_client_soap;

use ArrayAccess;
use Iterator;
use function base64_decode;
use function rewind;
use function unserialize;
use function urldecode;

/**
 * Implements SOAP complex argument mapping.
 *
 * @package Drupal\web_service_client_soap
 */
class ComplexArgument implements ArrayAccess, Iterator {

  protected $arg;

  /**
   * ComplexArgument constructor.
   *
   * @param array|string $arg
   *   Argument value. Support encoded and serialized strings.
   */
  public function __construct($arg) {
    if (is_string($arg)) {
      $arg = unserialize(base64_decode(urldecode($arg)));
    }
    $this->setArg($arg);
  }

  /**
   * Set argument value.
   *
   * @param array $arg
   *   Argument value.
   */
  public function setArg(array $arg) {
    $this->arg = $arg;
    reset($this->arg);
  }

  /**
   * Get argument value as an array.
   *
   * @return array
   *   The argument value.
   */
  public function toArray() {
    return $this->arg;
  }

  /**
   * {@inheritdoc}
   */
  public function current() {
    current($this->arg);
  }

  /**
   * {@inheritdoc}
   */
  public function next() {
    next($this->arg);
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    key($this->arg);
  }

  /**
   * {@inheritdoc}
   */
  public function valid() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    rewind($this->arg);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet($offset, $valor) {
    if (is_null($offset)) {
      $this->arg[] = $valor;
    }
    else {
      $this->arg[$offset] = $valor;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists($offset) {
    return isset($this->arg[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset($offset) {
    unset($this->arg[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet($offset) {
    return isset($this->arg[$offset]) ? $this->arg[$offset] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return urlencode(base64_encode(serialize($this->arg)));
  }

}
