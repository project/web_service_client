<?php

namespace Drupal\web_service_client_soap\Plugin\WsClient\WsType;

use DOMDocument;
use DOMXPath;
use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\web_service_client_soap\SoapResultRow;
use Drupal\web_service_client\WsClientTypePluginBase;
use Drupal\web_service_client_soap\Events\SoapEvents;
use Drupal\web_service_client_soap\Events\SoapTypedDataForm;
use Drupal\web_service_client_soap\Events\SoapTypedDataOptionsDefault;
use Drupal\web_service_client_soap\Generator;
use Exception;
use SoapClient;
use SoapFault;
use Wsdl2PhpGenerator\ComplexType;
use Wsdl2PhpGenerator\Config;
use Wsdl2PhpGenerator\Enum;
use XSLTProcessor;
use function file_get_contents;
use function str_replace;
use function strpos;
use function watchdog_exception;

/**
 * Plugin implementation of the 'gadget_base_field_widget' widget.
 *
 * @WsClientType(
 *   id = "soap",
 *   label = @Translation("SOAP"),
 *   views = {
 *     "query_plugin" = "web_service_client_soap_query",
 *     "relationship_plugin" = "web_service_client_soap_relationship",
 *     "filter_operation_plugin" =
 *   "web_service_client_soap_filter_operation_argument",
 *     "argument_operation_plugin" =
 *   "web_service_client_soap_contextual_filter_operation_argument",
 *     "field_plugins" = {
 *       "standard" = "web_service_client_soap_field_standard",
 *       "binary" = "web_service_client_soap_field_standard",
 *       "boolean" = "web_service_client_soap_field_boolean",
 *       "datetime_iso8601" = "web_service_client_soap_field_datetime",
 *       "duration_iso8601" = "web_service_client_soap_field_datetime",
 *       "email" = "web_service_client_soap_field_standard",
 *       "entity_reference" = "web_service_client_soap_field_standard",
 *       "float" = "web_service_client_soap_field_string",
 *       "integer" = "web_service_client_soap_field_string",
 *       "string" = "web_service_client_soap_field_string",
 *       "timespan" = "web_service_client_soap_field_numeric",
 *       "timestamp" = "web_service_client_soap_field_numeric",
 *       "uri" = "web_service_client_soap_field_string"
 *     },
 *     "argument_plugins" = {
 *       "standard" = "web_service_client_soap_argument_standard",
 *       "binary" = "web_service_client_soap_argument_standard",
 *       "boolean" = "web_service_client_soap_argument_standard",
 *       "datetime_iso8601" = "web_service_client_soap_argument_standard",
 *       "duration_iso8601" = "web_service_client_soap_argument_standard",
 *       "email" = "web_service_client_soap_argument_standard",
 *       "entity_reference" = "web_service_client_soap_argument_standard",
 *       "float" = "web_service_client_soap_argument_numeric",
 *       "integer" = "web_service_client_soap_argument_numeric",
 *       "string" = "web_service_client_soap_argument_standard",
 *       "timespan" = "web_service_client_soap_argument_numeric",
 *       "timestamp" = "web_service_client_soap_argument_numeric",
 *       "uri" = "web_service_client_soap_argument_standard"
 *     },
 *     "filter_plugins" = {
 *       "standard" = "web_service_client_soap_filter_standard",
 *       "binary" = "web_service_client_soap_filter_standard",
 *       "boolean" = "web_service_client_soap_filter_boolean",
 *       "datetime_iso8601" = "web_service_client_soap_filter_datetime",
 *       "duration_iso8601" = "web_service_client_soap_filter_datetime",
 *       "email" = "web_service_client_soap_filter_standard",
 *       "entity_reference" = "web_service_client_soap_filter_standard",
 *       "float" = "web_service_client_soap_filter_numeric",
 *       "integer" = "web_service_client_soap_filter_numeric",
 *       "string" = "web_service_client_soap_filter_string",
 *       "timespan" = "web_service_client_soap_filter_numeric",
 *       "timestamp" = "web_service_client_soap_filter_numeric",
 *       "uri" = "web_service_client_soap_filter_string"
 *     },
 *     "sort_plugins" = {
 *       "standard" = "web_service_client_soap_sort_standard",
 *       "binary" = "web_service_client_soap_sort_standard",
 *       "boolean" = "web_service_client_soap_sort_numeric",
 *       "datetime_iso8601" = "web_service_client_soap_sort_datetime",
 *       "duration_iso8601" = "web_service_client_soap_sort_datetime",
 *       "email" = "web_service_client_soap_sort_standard",
 *       "entity_reference" = "web_service_client_soap_sort_standard",
 *       "float" = "web_service_client_soap_sort_numeric",
 *       "integer" = "web_service_client_soap_sort_numeric",
 *       "string" = "web_service_client_soap_sort_string",
 *       "timespan" = "web_service_client_soap_sort_numeric",
 *       "timestamp" = "web_service_client_soap_sort_numeric",
 *       "uri" = "web_service_client_soap_sort_string"
 *     }
 *   }
 * )
 */
class WsClientSoapEndPoint extends WsClientTypePluginBase {

  /**
   * WSDL namespace
   *
   * @var string
   */
  const WSDL_NS = 'http://schemas.xmlsoap.org/wsdl/';

  /**
   * XML Schema namespace
   *
   * @see http://www.w3.org/TR/soap12-part1/#notation
   *
   * @var string
   */
  const SCHEMA_NS = 'http://www.w3.org/2001/XMLSchema';

  /**
   * The data types this web service offers.
   *
   * @var array
   */
  protected $dataTypes;

  /**
   * The enumerations this web service offers.
   *
   * @var array
   */
  protected $enumerations;

  /**
   * Get typed data options form.
   *
   * @param \Drupal\Core\TypedData\DataDefinition $dataType
   *   The typed data definition.
   * @param array $default_values
   *   Default values for the form.
   * @param array $context
   *   The context.
   *
   * @return mixed
   *   The typed data options form.
   */
  public static function getTypedDataOptionsForm(DataDefinition $dataType, array $default_values = [], array $context = []) {
    $form = [];

    $types = web_service_client_data_types();
    $types = $types['Others'];

    $type = $dataType->getDataType();
    $label = $dataType->getLabel();

    $required = $dataType->isRequired();
    $required = FALSE;
    $default = !empty($default_values[$label])
      ? $default_values[$label]
      : [];

    if ($type == 'list' || $type == 'web_service_client_list') {
      $item_definition = $dataType->getItemDefinition();
      $item_type = $item_definition->getDataType();

      if (!empty($types[$item_type])) {
        for ($i = 0; $i <= 3; $i++) {
          $form[$label][$i] = WsClientSoapEndPoint::typedDataElementMapper($item_type);
          $form[$label][$i] += [
            '#title' => $label,
            '#required' => $required,
            '#default_value' => !empty($default[$i]) ? $default[$i] : NULL,
          ];
        }
      }
      else {
        for ($i = 0; $i <= 3; $i++) {
          $form[$label][$i] = [
            '#type' => 'details',
            '#title' => $label,
            '#tree' => TRUE,
            '#open' => TRUE,
          ];

          $propertyDefinitions = $item_definition->getPropertyDefinitions();

          $default[$i] = (!empty($default) && !empty($default[$i])) ? $default[$i] : [];
          foreach ($propertyDefinitions as $key => $dataDefinition) {
            $form[$label][$i] +=
              WsClientSoapEndPoint::getTypedDataOptionsForm(
                $dataDefinition,
                $default[$i],
                $context
              );
          }
        }
      }
    }
    elseif (!empty($types[$type])) {
      $form[$label] = WsClientSoapEndPoint::typedDataElementMapper($type);
      $form[$label] += [
        '#title' => $label,
        '#required' => $required,
        '#default_value' => !empty($default) ? $default : NULL,
      ];

    }
    else {
      $form[$label] = [
        '#type' => 'details',
        '#title' => $label,
        '#tree' => TRUE,
        '#open' => TRUE,
      ];

      $propertyDefinitions = $dataType->getPropertyDefinitions();

      foreach ($propertyDefinitions as $key => $dataDefinition) {
        $form[$label] +=
          WsClientSoapEndPoint::getTypedDataOptionsForm(
            $dataDefinition,
            $default,
            $context
          );
      }
    }

    // Implements event dispatcher.
    $typedDataForm = new SoapTypedDataForm(
      $form,
      $dataType,
      $default_values,
      $context
    );
    $dispatcher = Drupal::service('event_dispatcher');
    $event = $dispatcher->dispatch(
      SoapEvents::CUSTOMIZE_FORM_ELEMENTS,
      $typedDataForm
    );

    return $event->getForm();
  }

  /**
   * Get data types mappings.
   *
   * @param string $type
   *   Data type.
   *
   * @return array
   *   The mapping array.
   */
  public static function typedDataElementMapper($type) {
    $primitive_types = [
      'string',
      'integer',
      'long',
      'float',
      'boolean',
      'double',
      'short',
      'decimal',
      'datetime_iso8601',
    ];

    $return = [];

    if (in_array($type, $primitive_types)) {
      switch ($type) {
        case 'double':
        case 'decimal':
        case 'integer':
        case 'long':
        case 'short':
          $return = ['#type' => 'number'];
          break;

        case 'string':
          $return = ['#type' => 'textfield'];
          break;

        case 'datetime_iso8601':
          $return = ['#type' => 'date'];
          break;

        case 'boolean':
          $return = ['#type' => 'checkbox'];
          break;
      }
    }

    return $return;
  }

  /**
   * Get typed data defined options.
   *
   * @param \Drupal\Core\TypedData\DataDefinition $dataType
   *   Typed data definition.
   * @param array $context
   *   The current context.
   *
   * @return mixed
   *   The typed data defined options.
   */
  public static function getTypedDataDefinedOptions(DataDefinition $dataType, array $context = []) {
    $cacheBackend = Drupal::service('cache.web_service_client');
    $type = $dataType->getDataType();
    $cid = $type . '|defined_options';
    $static_data = &drupal_static(__FUNCTION__ . ':' . $cid, NULL);
    if ($static_data !== NULL) {
      return $static_data;
    }
    if ($cache = $cacheBackend->get($cid)) {
      $static_data = $cache->data;
      return $cache->data;
    }

    $form = [];

    $types = web_service_client_data_types();
    $types = $types['Others'];

    $label = $dataType->getLabel();

    if (!empty($types[$type])) {
      $form[$label] = ['default' => ''];
    }
    else {
      $form[$label]['contains'] = [];

      $propertyDefinitions = $dataType->getPropertyDefinitions();

      foreach ($propertyDefinitions as $key => $dataDefinition) {
        $form[$label]['contains'] +=
          WsClientSoapEndPoint::getTypedDataDefinedOptions(
            $dataDefinition,
            $context
          );
      }
    }

    // Implements event dispatcher.
    $typedDataForm = new SoapTypedDataOptionsDefault($form, $dataType, $context);
    $dispatcher = Drupal::service('event_dispatcher');
    $event = $dispatcher->dispatch(SoapEvents::CUSTOMIZE_OPTIONS_DEFAULT, $typedDataForm);

    $return = $event->getDefaults();
    $static_data = $return;
    $cacheBackend->set($cid, $return);

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(&$form, FormStateInterface $form_state) {
    if (empty($this->service->getOperations()) && empty($this->service->getDataTypes())) {
      $this->initializeMetadata();
      $this->service->save();
    }

    Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
  }

  /**
   * Retrieve metadata from the WSDL about available data types and operations.
   *
   * @param bool $reset
   *   If TRUE, existing data types and operations will be overwritten.
   */
  public function initializeMetadata($reset = TRUE) {
    $client = $this->getClient();

    $this->enumerations = $this->parse_enumerations($client->__getTypes());

    $data_types = $this->parse_types($client->__getTypes());

    $this->dataTypes = $data_types;

    $operations = $this->parse_operations($client->__getFunctions());

    if ($reset) {
      $this->service->setDataTypes($data_types);
      $this->service->setOperations($operations);
    }
    else {
      $dt = $this->service->getDataTypes();
      $dt += $data_types;
      $this->service->setDataTypes($dt);

      $op = $this->service->getOperations();
      $op += $operations;
      $this->service->setOperations($op);
    }

    // @todo Remove: $this->service->clearCache();
  }

  /**
   * Get the SOAP client instance.
   *
   * @return \SoapClient
   *   The SOAP client instance.
   *
   * @throws \Exception
   */
  public function getClient() {
    if (!isset($this->client)) {
      $options['exceptions'] = TRUE;
      $global_parameters = $this->service->getGlobalParameters();
      $constants = get_defined_constants();
      foreach ($global_parameters as $name => $info) {
        $value = $info['default value'];
        if (ctype_upper($value)) {
          $options[$name] = ($value == 'TRUE' || $value == 'FALSE') ? filter_var($value, FILTER_VALIDATE_BOOLEAN) : $value;
        }
        else {
          $options[$name] = !empty($constants[$value]) ? constant($value) : $value;
        }
      }

      try {
        $this->client = new SoapClient($this->url, $options);
      } catch (SoapFault $e) {
        $msg = t('Error initializing SOAP client for service %name', ['%name' => $this->service->id()]);
        throw new Exception($msg);
      }
    }

    return $this->client;
  }

  /**
   * Convert enumerations.
   *
   * @param array $types
   *   The array containing the struct strings.
   *
   * @return array
   *   A enumerations array with property information.
   */
  public function parse_enumerations(array $types) {
    $wsclient_enunmerations = [];
    foreach ($types as $type_string) {
      if (strpos($type_string, 'struct') !== 0) {
        list($property_type, $type_name) = explode(' ', $type_string);
        // @todo Store enumeration options
        $wsclient_enunmerations[$type_name] = [
          'type' => $property_type,
        ];
      }
    }
    return $wsclient_enunmerations;
  }

  /**
   * Convert types.
   *
   * @param array $types
   *   The array containing the struct strings.
   *
   * @return array
   *   A data type array with property information.
   */
  public function parse_types(array $types) {
    $wsclient_types = [];
    foreach ($types as $type_string) {
      if (strpos($type_string, 'struct') === 0) {
        $parts = explode('{', $type_string);
        // Cut off struct and whitespaces from type name.
        $type_name = trim(substr($parts[0], 6));
        $wsclient_types[$type_name] = ['label' => $type_name];
        $property_string = $parts[1];
        // Cut off trailing '}'
        $property_string = substr($property_string, 0, -1);
        $properties = explode(';', $property_string);
        // Remove last empty element
        array_pop($properties);
        // Initialize empty property information.
        $wsclient_types[$type_name]['property info'] = [];
        foreach ($properties as $property_string) {
          // Cut off white spaces.
          $property_string = trim($property_string);
          $parts = explode(' ', $property_string);
          $property_type = $parts[0];
          $property_name = $parts[1];

          $wsclient_types[$type_name]['property info'][$property_name] = [
            'type' => !empty($this->enumerations[$property_type]) ? $this->enumerations[$property_type]['type'] : $this->type_mapper($property_type),
          ];
        }
      }
    }
    return $wsclient_types;
  }

  /**
   * Maps data type names from SOAPClient to wsclient/rules internals.
   */
  public function type_mapper($type) {
    $primitive_types = [
      'string',
      'int',
      'long',
      'float',
      'boolean',
      'double',
      'short',
      'decimal',
      'date',
      'dateTime',
    ];

    $dataType = $type;

    if (in_array($type, $primitive_types)) {
      switch ($type) {
        case 'double':
        case 'decimal':
          $dataType = 'float';
          break;

        case 'int':
        case 'long':
        case 'short':
          $dataType = 'integer';
          break;

        case 'date':
        case 'dateTime':
          $dataType = 'datetime_iso8601';
          break;
      }
    }

    // Check for list types.
    if (strpos($type, 'ArrayOf') === 0) {
      $type = substr($type, 7);
      $primitive = strtolower($type);

      if (in_array($primitive, $primitive_types)) {
        $dataType = 'list<' . $primitive . '>';
      }
      else {
        $dataType = 'list<' . $type . '>';
      }
    }

    // Otherwise return the type as is.
    return $dataType;
  }

  /**
   * Convert operations.
   *
   * @param array $operations
   *   The array containing the operation signature strings.
   *
   * @return array
   *   An operations array with parameter information.
   */
  public function parse_operations(array $operations) {
    $wsclient_operations = [];

    foreach ($operations as $operation) {
      $parts = explode(' ', $operation);
      $return_type = $this->type_mapper($parts[0]);
      $name_parts = explode('(', $parts[1]);
      $op_name = $name_parts[0];

      $wsclient_operations[$op_name] = [
        'label' => $op_name,
        'result' => ['type' => $return_type, 'label' => $return_type],
      ];

      $parts = explode('(', $operation);
      // Cut off trailing ')'.
      $param_string = substr($parts[1], 0, -1);

      if ($param_string) {
        $parameters = explode(',', $param_string);

        foreach ($parameters as $parameter) {
          $parameter = trim($parameter);
          $parts = explode(' ', $parameter);
          $param_type = $parts[0];
          // Remove leading '$' from parameter name.
          $param_name = substr($parts[1], 1);

          if ($param_type == $this->type_mapper($param_type)) {
            $wsclient_operations[$op_name]['parameter'][$param_name] = [
              'type' => !empty($this->dataTypes[$param_type]) ? $param_type : $this->enumerations[$param_type]['type'],
            ];
          }
          else {
            $wsclient_operations[$op_name]['parameter'][$param_name] = [
              'type' => $this->type_mapper($param_type),
            ];
          }
        }
      }
    }
    return $wsclient_operations;
  }

  public function updateDataTypes() {
    $this->explodeService();
    $old_data_types = $this->service->getDataTypes();
    $data_types = [];
    $typed_data = web_service_client_data_types();
    $this->primitiveTypes = $typed_data['Others'];
    $types = $this->generator->get('types');
    foreach ($types as $name => $type) {
      $this->explodeType($type, $data_types);
    }

    foreach ($data_types as $key => $value) {
      if (!empty($old_data_types[$key]['field_settings'])) {
        $data_types[$key]['field_settings'] = $old_data_types[$key]['field_settings'];
      }
    }

    $this->service->setDataTypes($data_types);
    $this->service->save();

    // Clear web_service_client cache.
    $cacheBackend = Drupal::service('cache.web_service_client');
    $cacheBackend->deleteAll();
  }

  public function explodeService() {
    $wsdl = $this->service->getEndpointUrl();
    $this->generator = new Generator();
    $config = new Config([
      'inputFile' => $wsdl,
      'outputDir' => '',
    ]);

    $this->generator->generate($config);
  }

  public function explodeType($type, &$data_types) {
    if (!($type instanceof ComplexType)) {
      return;
    }
    $types = $this->generator->get('types');
    $members = $type->getMembers();
    $name = $type->getIdentifier();
    $properties = [];
    if (!empty($members)) {
      foreach ($members as $key => $member) {
        $member_type = $member->getType();
        $multiple = strpos($member_type, '[]');
        $member_type = str_replace('[]', '', $member_type);
        $member_type = $this->type_mapper($member_type);
        if (empty($this->primitiveTypes[$member_type])) {
          if (!empty($types[$member_type]) && $types[$member_type] instanceof Enum) {
            $enum = $types[$member_type];
            $member_type = $enum->getDataType();
            $member_type = str_replace('[]', '', $member_type);
            $member_type = $this->type_mapper($member_type);
          }
        }
        $properties[$key] = [
          'type' => $member_type,
          'label' => $member->getName(),
          'multiple' => ($multiple === FALSE) ? FALSE : TRUE,
        ];
      }
    }

    $base_type = $type->getBaseType();
    if (!empty($base_type)) {
      $properties = $properties + $this->explodeType($base_type, $data_types);
    }

    $data_types[$name]['label'] = $name;
    $data_types[$name]['property info'] = $properties;

    return $properties;
  }

  /**
   * Convert extensions.
   *
   * @param array $types
   *   The array containing the struct strings.
   *
   * @return array
   *   A enumerations array with property information.
   */
  public function parse_extensions(string $name) {
    $result = [];

    $types = $this->generator->get('types');
    if (!empty($types[$name])) {
      $type = $types[$name];
      $members = $type->getMembers();
      if (!empty($members)) {
        foreach ($members as $key => $member) {
          $member_type = $member->getType();
          $multiple = strpos($member_type, '[]');
          $member_type = str_replace('[]', '', $member_type);
          if (empty($this->primitiveTypes[$member_type])) {
            $member_type = 'web_service_client:' . $this->service->id() . ':' . $member_type;
          }
          $result[$key] = [
            'type' => $member_type,
            'label' => $member->getName(),
            'multiple' => $multiple,
          ];
        }
      }
      $base_type = $type->getBaseType();
      if (!empty($base_type)) {
        $identifier = $base_type->getIdentifier();
        $result = $result + $this->parse_extensions($identifier);
      }
    }

    return $result;
  }

  /**
   * Calls the SOAP service.
   *
   * @param string $operation
   *   The name of the operation to execute.
   * @param array $arguments
   *   Arguments to pass to the service with this operation.
   * @param bool $clean
   *   If TRUE arguments are cleaned.
   *
   * @return mixed
   *   The SOAP response.
   *
   * @throws \Exception
   */
  public function call($operation, array $arguments, $clean = FALSE) {
    $client = $this->getClient();
    try {
      if ($clean) {
        foreach ($arguments[0] as &$arg) {
          WsClientSoapEndPoint::cleanEmptyCallArguments($arg);
        }
      }
      $response = $client->__soapCall($operation, $arguments);

      return $response;
    } catch (Exception $e) {
      watchdog_exception('web_service_client_soap', $e, 'Error invoking %op from SOAP service %name: @error', [
        '@error' => $e->getMessage(),
        '%name' => $this->service->label(),
        '%op' => $operation,
      ]);
      throw $e;
    }
  }

  /**
   * Clean call arguments.
   *
   * @param mixed $arguments
   *   The method call arguments.
   */
  public static function cleanEmptyCallArguments(&$arguments) {
    if (!is_array($arguments)) {
      return;
    }

    foreach ($arguments as $key => $value) {
      if (is_array($arguments[$key])) {
        WsClientSoapEndPoint::cleanEmptyCallArguments($arguments[$key]);
      }

      if (is_numeric($arguments[$key]) || !empty($arguments[$key])) {
        continue;
      }

      unset($arguments[$key]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionExtraBase($op_name) {
    $extra = [];
    // @todo Remove: $extra['xsl'] = $this->buildXslDocument($service, $table_key, $prop_key, $properties);
    $definition = $this->pluginDefinition;
    $data_types = $this->service->getDataTypes();
    $operations = $this->service->getOperations();
    $operation = $operations[$op_name];

    $result_type = $operation['result']['type'];
    $result_data_type = key($data_types[$result_type]['property info']);

    $template = $definition['definition_path'] . '/xsl-templates/base-table.xsl';
    $xls_string = file_get_contents($template);
    $find = ['__base_result_type__', '__table_key__'];
    $replace = [$result_type, $result_data_type];
    $xls_string = str_replace($find, $replace, $xls_string);

    $property_type = $data_types[$result_type]['property info'][$result_data_type]['type'];

    $extra['xsl'] = $xls_string;
    $extra['result_name'] = $result_data_type;
    $extra['result_type'] = $result_type;
    $extra['property_type'] = $property_type;

    return $extra;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionExtraRelationship($table_key, $data_type) {
    $extra = [];
    // @todo Remove: $extra['xsl'] = $this->buildXslDocument($service, $table_key, $prop_key, $properties);
    $definition = $this->pluginDefinition;

    $template = $definition['definition_path'] . '/xsl-templates/table.xsl';
    $xls_string = file_get_contents($template);
    $find = ['__table_key__'];
    $replace = [$table_key];
    $xls_string = str_replace($find, $replace, $xls_string);

    $extra['xsl'] = $xls_string;
    $extra['result_name'] = $table_key;
    $extra['result_type'] = $data_type;

    return $extra;
  }

  /**
   * {@inheritdoc}
   */
  public function dataProcessOperationsAlter(&$properties, $result_type, $operations, $data_types, $primitive_types) {
    $result_data_type = key($data_types[$result_type]['property info']);
    $property_info_type = $data_types[$result_type]['property info'][$result_data_type]['type'];

    // Process the result.
    if (!empty($data_types[$property_info_type])) {
      $properties = $data_types[$property_info_type]['property info'];
    }
    else {
      $properties = [
        $result_data_type => ['type' => $property_info_type],
      ];
    }
  }

}
