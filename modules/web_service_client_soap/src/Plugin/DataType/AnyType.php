<?php

namespace Drupal\web_service_client_soap\Plugin\DataType;

use Drupal\Core\TypedData\TypedData;

/**
 * The "anyType" soap data type.
 *
 * The "anyType" data type does not implement a list or complex data interface,
 * nor is it mappable to any primitive type. Thus, it may contain any PHP data
 * for which no further metadata is available.
 *
 * @DataType(
 *   id = "anyType",
 *   label = @Translation("Any data")
 * )
 */
class AnyType extends TypedData {

  /**
   * The data value.
   *
   * @var mixed
   */
  protected $value;

}
