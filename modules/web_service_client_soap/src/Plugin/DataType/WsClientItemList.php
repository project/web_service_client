<?php

namespace Drupal\web_service_client_soap\Plugin\DataType;

use Drupal\web_service_client\Plugin\DataType\WsClientItemList as ParentWsClientItemList;

/**
 * Defines the base plugin for deriving data types for wsclient.
 */
class WsClientItemList extends ParentWsClientItemList {

}
