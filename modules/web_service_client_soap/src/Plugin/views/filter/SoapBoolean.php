<?php

namespace Drupal\web_service_client_soap\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\BooleanOperator;
use Drupal\views_xml_backend\Plugin\views\filter\XmlFilterInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * Default implementation of the base filter plugin.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("web_service_client_soap_filter_boolean")
 */
class SoapBoolean extends BooleanOperator implements XmlFilterInterface {

  use SoapFilterHelperTrait;
  use AdminLabelTrait;

  public function queryOpBoolean($field) {
    if ($this->operator == '=') {
      return "$field = 'true'";
    }
    else {
      return "$field = 'false'";
    }
  }

  /**
   * Returns an array of operator information.
   *
   * @return array
   *   An array of operator information.
   */
  public function operators() {
    return [
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'queryOpBoolean',
        'short' => $this->t('='),
        'values' => 1,
        'query_operator' => static::EQUAL,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'queryOpBoolean',
        'short' => $this->t('!='),
        'values' => 1,
        'query_operator' => static::NOT_EQUAL,
      ],
    ];
  }

}
