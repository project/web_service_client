<?php

namespace Drupal\web_service_client_soap\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\StringFilter;
use Drupal\views_xml_backend\Plugin\views\filter\XmlFilterInterface;
use Drupal\views_xml_backend\Xpath;
use Drupal\web_service_client\TypedData\WsClientDataDefinition;
use Drupal\web_service_client_soap\AdminLabelTrait;
use Drupal\web_service_client_soap\Plugin\WsClient\WsType\WsClientSoapEndPoint;

/**
 * Default implementation of the base filter plugin.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("web_service_client_soap_filter_operation_argument")
 */
class SoapFilterOperation extends StringFilter implements XmlFilterInterface {

  use AdminLabelTrait;

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    // @fixme tl;dr
    if (TRUE) {
      return '';
    }

    if ($this->isAGroup()) {
      return $this->t('grouped');
    }

    if (!empty($this->options['exposed'])) {
      return $this->t('exposed');
    }

    $options = $this->operatorOptions('short');
    $output = '';

    if (!empty($options[$this->operator])) {
      $output = $options[$this->operator];
    }

    if (in_array($this->operator, $this->operatorValues(1))) {
      $output .= ' ' . $this->value;
    }

    return $output;
  }

  /**
   * Provide the basic form which calls through to subforms.
   *
   * If overridden, it is best to call through to the parent,
   * or to at least make sure all of the functions in this form
   * are called.
   *
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $extra = $this->configuration['web_service_client_extra'];
    $typedData = $extra['typedData'];
    list($x, $y, $parameterType) = explode(':', $typedData);
    $data = WsClientDataDefinition::create($typedData);
    $data->setLabel($parameterType);

    $context = [
      'scope' => $this,
      'function' => __FUNCTION__,
      'form_state' => $form_state,
    ];
    $form_value =
      WsClientSoapEndPoint::getTypedDataOptionsForm(
        $data,
        $this->options['value'],
        $context
      );

    if (is_array($form_value)) {
      $form['value'] += $form_value;
      $form['value']['#type'] = 'container';
      unset($form['value']['#title']);
      $form['operator']['#type'] = 'hidden';
    }
  }

  /**
   * Simple validate handler.
   *
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    $this->operatorValidate($form, $form_state);
    $this->valueValidate($form, $form_state);

    if (!empty($this->options['exposed']) && !$this->isAGroup()) {
      $this->validateExposeForm($form, $form_state);
    }

    if ($this->isAGroup()) {
      $this->buildGroupValidate($form, $form_state);
    }
  }

  /**
   * Render our chunk of the exposed filter form when selecting.
   *
   * You can override this if it doesn't do what you expect.
   *
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    parent::buildExposedForm($form, $form_state);

    $extra = $this->configuration['web_service_client_extra'];
    $typedData = $extra['typedData'];
    list($x, $y, $parameterType) = explode(':', $typedData);
    $data = WsClientDataDefinition::create($typedData);
    $data->setLabel($parameterType);

    $context = [
      'scope' => $this,
      'function' => __FUNCTION__,
      'form_state' => $form_state,
    ];
    $form_value =
      WsClientSoapEndPoint::getTypedDataOptionsForm(
        $data,
        $this->options['value'],
        $context
      );

    if (!empty($this->options['expose']['identifier'])) {
      $this->valueForm($form, $form_state);
    }

    if (is_array($form_value)) {
      $form['value'] += $form_value;
      $form['value']['#type'] = 'container';
      unset($form['value']['#title']);
      $form['operator']['#type'] = 'hidden';
    }
  }

  /**
   * Options form subform for setting options.
   *
   * This should be overridden by all child classes and it must
   * define $form['value']
   *
   * @see buildOptionsForm()
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateExposeForm($form, FormStateInterface $form_state) {
    parent::validateExposeForm($form, $form_state);
  }

  /**
   * Simple submit handler.
   *
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    // Do not store these values.
    $form_state->unsetValue('expose_button');
    $form_state->unsetValue('group_button');

    if (!$this->isAGroup()) {
      $this->operatorSubmit($form, $form_state);
      $this->valueSubmit($form, $form_state);
    }

    if (!empty($this->options['exposed'])) {
      $this->submitExposeForm($form, $form_state);
    }

    if ($this->isAGroup()) {
      $this->buildGroupSubmit($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function storeExposedInput($input, $status) {
    parent::storeExposedInput($input, $status);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->query->operationFilterCriteria = $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    $operator = $this->operator;
    $xpath = $this->options['xpath_selector'];
    $value = Xpath::escapeXpathString($this->value);

    if ($operator === '=' || $operator === '!=') {
      return "$xpath $operator $value";
    }

    if (strpos($operator, '!') === 0) {
      $operator = ltrim($operator, '!');
      return "not($operator($xpath, $value))";
    }

    return "$operator($xpath, $value)";
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $typedData = $this->configuration['web_service_client_extra']['typedData'];
    list($x, $y, $parameterType) = explode(':', $typedData);
    $data = WsClientDataDefinition::create($typedData);
    $data->setLabel($parameterType);

    unset($options['value']['default']);
    $options['value']['contains'] =
      WsClientSoapEndPoint::getTypedDataDefinedOptions($data);

    return $options;
  }

}
