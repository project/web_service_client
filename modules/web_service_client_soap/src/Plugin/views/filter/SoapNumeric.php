<?php

namespace Drupal\web_service_client_soap\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\NumericFilter;
use Drupal\views_xml_backend\Plugin\views\filter\XmlFilterInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * Default implementation of the base filter plugin.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("web_service_client_soap_filter_numeric")
 */
class SoapNumeric extends NumericFilter implements XmlFilterInterface {

  use SoapFilterHelperTrait;
  use AdminLabelTrait;

  public function opSimple($field) {
    switch ($this->operator) {
      case '<':
        return "{$this->value['value']} > number($field)";

      case '<=':
        return "{$this->value['value']} >= number($field)";

      default:
        return "number($field) {$this->operator} {$this->value['value']}";
    }
  }

  public function opBetween($field) {
    if ($this->operator == 'between') {
      return "(number($field) >= {$this->value['min']} and {$this->value['max']} >= number($field))";
    }
    else {
      return "not((number($field) >= {$this->value['min']} and {$this->value['max']} >= number($field)))";
    }
  }

  function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'opSimple',
        'short' => $this->t('<'),
        'values' => 1,
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('<='),
        'values' => 1,
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'opSimple',
        'short' => $this->t('='),
        'values' => 1,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'opSimple',
        'short' => $this->t('!='),
        'values' => 1,
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('>='),
        'values' => 1,
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'opSimple',
        'short' => $this->t('>'),
        'values' => 1,
      ],
      'between' => [
        'title' => $this->t('Is between'),
        'method' => 'opBetween',
        'short' => $this->t('between'),
        'values' => 2,
      ],
      'not between' => [
        'title' => $this->t('Is not between'),
        'method' => 'opBetween',
        'short' => $this->t('not between'),
        'values' => 2,
      ],
    ];

    // If the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += [
        'empty' => [
          'title' => $this->t('Is empty (NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('empty'),
          'values' => 0,
        ],
        'not empty' => [
          'title' => $this->t('Is not empty (NOT NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('not empty'),
          'values' => 0,
        ],
      ];
    }

    return $operators;
  }

}
