<?php

namespace Drupal\web_service_client_soap\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;
use Drupal\views_xml_backend\Plugin\views\filter\XmlFilterInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * Default implementation of the base filter plugin.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("web_service_client_soap_filter_string")
 */
class SoapString extends StringFilter implements XmlFilterInterface {

  use SoapFilterHelperTrait;
  use AdminLabelTrait;
}
