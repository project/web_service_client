<?php

namespace Drupal\web_service_client_soap\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\Standard;
use Drupal\views_xml_backend\Plugin\views\filter\XmlFilterInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * Default implementation of the base filter plugin.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("web_service_client_soap_filter_standard")
 */
class SoapStandard extends Standard implements XmlFilterInterface {

  use SoapFilterHelperTrait;
  use AdminLabelTrait;


  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

}
