<?php

namespace Drupal\web_service_client_soap\Plugin\views\filter;

use Drupal\views_xml_backend\Xpath;

/**
 * A handler to provide an XML text field.
 */
trait SoapFilterHelperTrait {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This is done only for code completion.
    /** @var \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin $query */
    $query = &$this->query;
    $operators = $this->operators();
    $method = $operators[$this->options['operator']]['method'];

    $query->addFilterXpath(
      $this->options['id'],
      [
        'xpath' => call_user_func_array([
          $this,
          $method,
        ], [$this->options['field']]),
        'group' => $this->options['group'],
      ]);
  }

  /**
   * Operators definition.
   *
   * This kind of construct makes it relatively easy for a child class
   * to add or remove functionality by overriding this function and
   * adding/removing items from this array.
   *
   * @return array
   *   An array of operators definition.
   */
  public function operators() {
    $operators = [
      '=' => [
        'title' => $this->t('Is equal to'),
        'short' => $this->t('='),
        'method' => 'opEqual',
        'values' => 1,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'short' => $this->t('!='),
        'method' => 'opEqual',
        'values' => 1,
      ],
      'contains' => [
        'title' => $this->t('Contains'),
        'short' => $this->t('contains'),
        'method' => 'opContains',
        'values' => 1,
      ],
      'word' => [
        'title' => $this->t('Contains any word'),
        'short' => $this->t('has word'),
        'method' => 'opContainsWord',
        'values' => 1,
      ],
      'allwords' => [
        'title' => $this->t('Contains all words'),
        'short' => $this->t('has all'),
        'method' => 'opContainsWord',
        'values' => 1,
      ],
      'starts' => [
        'title' => $this->t('Starts with'),
        'short' => $this->t('begins'),
        'method' => 'opStartsWith',
        'values' => 1,
      ],
      'not_starts' => [
        'title' => $this->t('Does not start with'),
        'short' => $this->t('not_begins'),
        'method' => 'opStartsWith',
        'values' => 1,
      ],
      'ends' => [
        'title' => $this->t('Ends with'),
        'short' => $this->t('ends'),
        'method' => 'opEndsWith',
        'values' => 1,
      ],
      'not_ends' => [
        'title' => $this->t('Does not end with'),
        'short' => $this->t('not_ends'),
        'method' => 'opEndsWith',
        'values' => 1,
      ],
      'not' => [
        'title' => $this->t('Does not contain'),
        'short' => $this->t('!has'),
        'method' => 'opContains',
        'values' => 1,
      ],
      'shorterthan' => [
        'title' => $this->t('Length is shorter than'),
        'short' => $this->t('shorter than'),
        'method' => 'opShorterThan',
        'values' => 1,
      ],
      'longerthan' => [
        'title' => $this->t('Length is longer than'),
        'short' => $this->t('longer than'),
        'method' => 'opLongerThan',
        'values' => 1,
      ],
      'any in' => [
        'title' => $this->t('Is one of'),
        'short' => $this->t('='),
        'logic operator' => 'or',
        'operator' => '=',
        'method' => 'opSimple',
        'values' => 1,
      ],
      'all in' => [
        'title' => $this->t('Is all of'),
        'short' => $this->t('='),
        'logic operator' => 'and',
        'operator' => '=',
        'method' => 'opSimple',
        'values' => 1,
      ],
      'any not in' => [
        'title' => $this->t('Is not one of'),
        'short' => $this->t('!='),
        'logic operator' => 'or',
        'operator' => '!=',
        'method' => 'opSimple',
        'values' => 1,
      ],
      'all not in' => [
        'title' => $this->t('Is not all of'),
        'short' => $this->t('!='),
        'logic operator' => 'and',
        'operator' => '!=',
        'method' => 'opSimple',
        'values' => 1,
      ],
    ];

    // If the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += [
        'empty' => [
          'title' => $this->t('Is empty (NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('empty'),
          'values' => 0,
        ],
        'not empty' => [
          'title' => $this->t('Is not empty (NOT NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('not empty'),
          'values' => 0,
        ],
      ];
    }

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    $operator = $this->operator;
    $xpath = $this->options['xpath_selector'];
    $value = Xpath::escapeXpathString($this->value);

    if ($operator === '=' || $operator === '!=') {
      return $xpath . ' ' . $operator . ' ' . $value;
    }

    if (strpos($operator, '!') === 0) {
      $operator = ltrim($operator, '!');
      return 'not(' . $operator . '(' . $xpath . ',' . $value . '))';
    }

    return $operator . '(' . $xpath . ',' . $value . ')';
  }

  public function opSimple($field) {
    if (is_array($this->value)) {
      $values = $this->value;
    }
    else {
      $values = explode(',', $this->value);
      if (count($values) == 1) {
        $values = explode('+', $this->value);
      }
    }
    $result = [];
    $operators = $this->operators();
    $operator = $operators[$this->operator];
    foreach ($values as $value) {
      $result[] = $field . $operator['operator'] . '\'' . $value . '\'';
    }
    return '(' . implode(' ' . $operator['logic operator'] . ' ', $result) . ')';
  }

  /**
   * Equality comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opEqual($field) {
    return $field . $this->operator . '\'' . $this->value . '\'';
  }

  /**
   * Contains comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opContains($field) {
    return ($this->operator == 'contains')
      ? $field . '[contains(text(), \'' . $this->value . '\')]'
      : $field . '[not(contains(text(), \'' . $this->value . '\'))]';
  }

  /**
   * Contains words comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opContainsWord($field) {
    $xpath = NULL;
    $words = explode(' ', $this->value);
    $log_op = ($this->operator == 'word') ? 'or' : 'and';

    foreach ($words as $word) {
      $xpath .= $log_op . $field . '[contains(text(), \'' . $word . '\')]';
    }

    $xpath = '(' . $xpath . ')';

    return $xpath;
  }

  /**
   * Starts-with comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opStartsWith($field) {
    return ($this->operator == 'starts')
      ? $field . '[starts-with(text(), \'' . $this->value . '\')]'
      : $field . '[not(starts-with(text(), \'' . $this->value . '\'))]';
  }

  /**
   * Ends-with comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opEndsWith($field) {
    return ($this->operator == 'ends')
      // For XPath 2.0 use:
      // > $field . '[ends-with(text(), \'' . $this->value . '\')]'.
      ? $field . '[substring(text(), string-length(text()) - string-length(\'' . $this->value . '\') +1) = \'' . $this->value . '\']'
      // For XPath 2.0 use:
      // > $field . '[not(ends-with(text(), \'' . $this->value . '\'))]'.
      : $field . '[not(substring(text(), string-length(text()) - string-length(\'' . $this->value . '\') +1) = \'' . $this->value . '\')]';
  }

  /**
   * Shorter-than comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opShorterThan($field) {
    return $field . '[string-length(text()) < \'' . $this->value . '\']';
  }

  /**
   * Longer-than comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opLongerThan($field) {
    return $field . '[string-length(text()) > \'' . $this->value . '\']';
  }

  /**
   * Empty comparison.
   *
   * @param string $field
   *   The field name.
   *
   * @return string
   *   The comparison string.
   */
  public function opEmpty($field) {
    return ($this->operator == 'empty')
      ? $field . '[string-length(text()) = 0]'
      : $field . '[string-length(text()) > 0]';
  }

}
