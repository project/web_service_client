<?php

namespace Drupal\web_service_client_soap\Plugin\views\query;

use DOMDocument;
use DOMNode;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Drupal\views_xml_backend\Messenger;
use Drupal\views_xml_backend\MessengerInterface;
use Drupal\views_xml_backend\Plugin\views\argument\XmlArgumentInterface;
use Drupal\views_xml_backend\Plugin\views\filter\Date;
use Drupal\views_xml_backend\Plugin\views\filter\Numeric;
use Drupal\views_xml_backend\Plugin\views\filter\Standard;
use Drupal\views_xml_backend\Plugin\views\query\Xml;
use Drupal\web_service_client\Entity\WsClient;
use Drupal\web_service_client\Module;
use Drupal\web_service_client\TypedData\WsClientDataDefinition;
use Drupal\web_service_client_soap\SoapResultRow;
use Drupal\web_service_client_soap\ComplexArgument;
use Drupal\web_service_client_soap\Plugin\WsClient\WsType\WsClientSoapEndPoint;
use Exception;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XSLTProcessor;
use function watchdog_exception;

/**
 * Views query plugin for a SOAP endpoint.
 *
 * @ingroup views_query_plugins
 *
 * @ViewsQuery(
 *   id = "web_service_client_soap_query",
 *   title = @Translation("WS Client Query"),
 *   help = @Translation("Query will be generated and run SOAP web service.")
 * )
 */
class WsClientSoapQueryPlugin extends Xml {

  public $operation;

  public $operationFilterCriteria;

  public $operationContextualFilter;

  public $operationArguments;

  /**
   * An array XPath selectors.
   *
   * @var array
   */
  protected $filterXpath;

  /**
   * An array XPath selectors.
   *
   * @var array
   */
  protected $contextualFilterXpath;

  /**
   * An array of XPath selectors.
   *
   * @var array
   */
  protected $argumentXpath;

  /**
   * Complex type processor.
   *
   * @var \XSLTProcessor
   */
  protected $xslRelationshipTransform;

  /**
   * Sorting processor.
   *
   * @var \XSLTProcessor
   */
  protected $xslSortTransform;

  /**
   * Views arguments processor (currently unused).
   *
   * @var \XSLTProcessor
   */
  protected $xslArgumentTransform;

  /**
   * Filtering processor.
   *
   * @var \XSLTProcessor
   */
  protected $xslFilterTransform;

  /**
   * Filtering processor.
   *
   * @var \XSLTProcessor
   */
  protected $xslContextualFilterTransform;

  /**
   * Base table processor.
   *
   * @var \XSLTProcessor
   */
  protected $xslBaseTableTransform;

  /**
   * A web service client instance.
   *
   * @var \Drupal\web_service_client\Entity\WsClient
   */
  protected $service;

  /**
   * An array of XSLT templates for SOAP complex types.
   *
   * @var array
   */
  protected $xslRelationships;

  /**
   * A XPath string to filter results.
   *
   * @var string
   */
  protected $xslFilters;

  /**
   * A XPath string to filter results.
   *
   * @var string
   */
  protected $xslContextualFilters;

  /**
   * An array of XSLT templates for sorting.
   *
   * @var array
   */
  protected $xslSorts;

  /**
   * Endpoint instance used by Views.
   *
   * @var \Drupal\web_service_client_soap\Plugin\WsClient\WsType\WsClientSoapEndPoint
   */
  protected $endpoint;

  /**
   * Typed data manager service.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * WsClientSoapQueryPlugin constructor.
   *
   * @inheritdoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, CacheBackendInterface $cache_backend, LoggerInterface $logger, TypedDataManagerInterface $typed_data_manager, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client, $cache_backend, $logger, $messenger);
    $this->xslRelationships = [];
    $this->filterXpath = [];
    $this->xslFilters = '';
    $this->xslContextualFilters = '';
    $this->xslSorts = [];
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('cache.views_xml_backend_download'),
      $container->get('logger.factory')->get('views_xml_backend'),
      $container->get('typed_data_manager'),
      new Messenger()
    );
  }

  /**
   * Get the arguments of the View.
   *
   * @return array
   *   A XPath strings array.
   */
  public function getArgumentXpath() {
    return $this->argumentXpath;
  }

  /**
   * Sets the arguments for the View.
   *
   * @param array $argumentXpath
   *   A XPath string array.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setArgumentXpath(array $argumentXpath) {
    $this->argumentXpath = $argumentXpath;
    return $this;
  }

  /**
   * Adds a argument to the view. This method give support for views_xml_backend
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function addArgument(XmlArgumentInterface $argument) {
    $xpath = $argument->options['xpath_selector'] . '=\'' . $argument->getValue() . '\'';

    $this->addContextualFilterXpath(
      $argument->options['id'],
      [
        'xpath' => $xpath,
      ]);
  }

  /**
   * Adds a filter to the view.
   *
   * @param string|int $id
   *   The filter ID.
   * @param array $options
   *   Filter definition.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function addContextualFilterXpath($id, array $options) {
    $this->contextualFilterXpath[$id] = $options;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $sourceDefaultValue = isset($this->options['source'])
      ? $this->options['source']
      : 'query';

    $form['source'] = [
      '#type' => 'radios',
      '#title' => $this->t('Source arguments'),
      '#options' => [
        'query' => $this->t('Query'),
        'filter' => $this->t('Filter'),
        'argument' => $this->t('Argument'),
      ],
      '#default_value' => $sourceDefaultValue,
    ];

    $form['arguments'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="query[options][source]"]' => ['value' => 'query'],
        ],
      ],
    ];

    $typedData = $this->view->storage->get('base_table');
    list($x, $y, $parameterType) = explode(':', $typedData);
    $data = WsClientDataDefinition::create($typedData);
    $data->setLabel($parameterType);

    $formValue = WsClientSoapEndPoint::getTypedDataOptionsForm(
      $data,
      $this->options['arguments']
    );

    if (is_array($formValue)) {
      $form['arguments'] += $formValue;
    }

    $filterLabel = $parameterType . ' ' . t('(Operation Parameter)');
    $filterMarkup = $this->t(
      '%before<strong>%filter</strong>%after',
      [
        '%before' => 'You must add and fill this filter criteria:',
        '%filter' => $filterLabel,
        '%after' => ' for works this query',
      ]
    );
    $argumentMarkup = $this->t(
      '%before<strong>%filter</strong>%after',
      [
        '%before' => 'You must add and this contextual filter:',
        '%filter' => $filterLabel,
        '%after' => ' for works this query',
      ]
    );

    $form['filter_message'] = [
      '#type' => 'item',
      '#markup' => $filterMarkup,
      '#states' => [
        'visible' => [
          ':input[name="query[options][source]"]' => ['value' => 'filter'],
        ],

      ],
    ];

    $form['argument_message'] = [
      '#type' => 'item',
      '#markup' => $argumentMarkup,
      '#states' => [
        'visible' => [
          ':input[name="query[options][source]"]' => ['value' => 'argument'],
        ],

      ],
    ];

    $form['global_xpath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Global xpath'),
      '#default_value' => $this->options['global_xpath'],
      '#description' => 'xpath for get global elements in all rows, for example: row/productInfo/code',
    ];

    $form['show_errors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show XML errors'),
      '#default_value' => $this->options['show_errors'],
      '#description' => $this->t('If there were any errors during XML parsing, display them. It is recommended to leave this on during development.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(ViewExecutable $view) {
    $this->view = $view;
    $view->initPager();

    // Let the pager modify the query to add limits.
    $view->pager->query();

    $view->build_info['query'] = $this->query();
    $view->build_info['count_query'] = '';
    $this->livePreview = !empty($view->live_preview);

    $arguments = [];
    switch ($this->options['source']) {
      case 'query':
        $arguments = array_shift($this->options['arguments']);
        break;

      case 'filter':
        $arguments = $this->operationFilterCriteria;
        break;

      case 'argument':
        $arguments = $this->operationContextualFilter;
        break;

    }
    $this->operationArguments = ($arguments instanceof ComplexArgument) ? $arguments->toArray() : $arguments;
  }

  /**
   * {@inheritdoc}
   */
  public function query($get_count = FALSE) {
    list($module, $service_name, $operation) =
      explode(':', $this->view->storage->get('base_table'));
    $this->operation = $operation;
    $this->service = WsClient::load($service_name);
    $this->endpoint = $this->service->getEndpoint();

    $this->applyXslBaseTableTransform();

    if (!empty($this->xslRelationships)) {
      $this->applyXslRelationshipTransform();
    }

    // Support for views_xml_backend
    if (!empty($this->filters)) {
      $this->addViewsXmlBackendFilter();
    }

    if (!empty($this->filterXpath)) {
      $this->applyXslFilterTransform();
    }

    if (!empty($this->contextualFilterXpath)) {
      $this->applyXslContextualFilterTransform();
    }

    if (!empty($this->xslSorts)) {
      $this->applyXslSortTransform();

    }

    return [];
  }

  /**
   * Apply XSLT template for base_table.
   */
  public function applyXslBaseTableTransform() {
    $base_table_info = $this->getBaseTableInfoTemplate();
    $trail = '/' . $base_table_info['result_name'];
    $this->xslBaseTableTransform = $this
      ->getProcessor($base_table_info['xsl']);

    $this->xslBaseTableTransform->setParameter('', 'parent-data-type', $base_table_info['result_type']);
    $this->xslBaseTableTransform->setParameter('', 'parent-name', $base_table_info['result_name']);
    $this->xslBaseTableTransform->setParameter('', 'parent-trail', $trail);
  }

  /**
   * Get info template for base_table.
   *
   * @return array
   *   The XSLT template from the views data definition.
   */
  public function getBaseTableInfoTemplate() {
    $base_table = $this->view->storage->get('base_table');
    $data = Views::viewsData()->getAll();

    return $data[$base_table]['table']['base']['web_service_client_extra'];
  }

  /**
   * Get XSLT processor instance with XSL styles loaded.
   *
   * @param string $xls_string
   *   The styles to apply.
   *
   * @return \XSLTProcessor
   *   The processor instance.
   *
   * @deprecated To be replaced by WsClientSoapQueryPlugin::applyXsltTemplate()
   */
  public function getProcessor($xls_string) {
    $xsl_document = new DOMDocument();
    $xsl_document->loadXML($xls_string);

    $xsl = new XSLTProcessor();
    $xsl->importStyleSheet($xsl_document);

    return $xsl;
  }

  /**
   * Apply XSLT template for complex types as relationship.
   *
   * @param string $parent
   *   The parent node.
   */
  public function applyXslRelationshipTransform($parent = 'none', $trail = '') {
    $base_table = $this->getBaseTableInfoTemplate();
    foreach ($this->xslRelationships as $id => $relationship) {
      if ($relationship['parent'] == $parent) {
        $trail = ($parent == 'none') ? '/' . $base_table['result_name'] . '/' . $relationship['name'] : $trail . '/' . $relationship['name'];
        $this->xslRelationshipTransform[$id] =
          $this->getProcessor($relationship['xsl']);

        $this->xslRelationshipTransform[$id]->setParameter('', 'parent-data-type', $relationship['type']);
        $this->xslRelationshipTransform[$id]->setParameter('', 'parent-name', $relationship['name']);
        $this->xslRelationshipTransform[$id]->setParameter('', 'parent-trail', $trail);
        $this->applyXslRelationshipTransform($id, $trail);
      }
    }
  }

  public function addViewsXmlBackendFilter() {
    foreach ($this->filters as $filter) {
      if (empty($filter->options['xpath_selector'])) {
        continue;
      }

      $plugin = NULL;
      switch (TRUE) {
        case $filter instanceof Standard:
          $plugin = Views::pluginManager('filter')
            ->createInstance('web_service_client_soap_filter_standard');
          break;

        case $filter instanceof Numeric:
          $plugin = Views::pluginManager('filter')
            ->createInstance('web_service_client_soap_filter_numeric');
          break;

        case $filter instanceof Date:
          $plugin = Views::pluginManager('filter')
            ->createInstance('web_service_client_soap_filter_date');
          break;

        default:
          $plugin = Views::pluginManager('filter')
            ->createInstance('web_service_client_soap_filter_standard');
      }

      $plugin->value = $filter->value;
      $operators = $plugin->operators();
      $method = $operators[$filter->options['operator']]['method'];
      $this->addFilterXpath(
        $filter->options['id'],
        [
          'xpath' => call_user_func_array([
            $plugin,
            $method,
          ], [$filter->options['xpath_selector']]),
          'group' => $filter->options['group'],
        ]);
    }
  }

  /**
   * Adds a filter to the view.
   *
   * @param string|int $id
   *   The filter ID.
   * @param array $options
   *   Filter definition.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function addFilterXpath($id, array $options) {
    $this->filterXpath[$id] = $options;

    return $this;
  }

  /**
   * Apply XSLT template to filter XML result.
   */
  public function applyXslFilterTransform() {
    $filterXpath = [];

    foreach ($this->where as $group_id => $group) {
      $groupFilter = [];

      foreach ($this->filterXpath as $filter_id => $filter) {
        if ($filter['group'] == $group_id) {
          $groupFilter[] = $filter['xpath'];
        }
      }

      $key = ' ' . strtolower($group['type']) . ' ';
      $filterXpath[] = '(' . implode($key, $groupFilter) . ')';
    }

    $groupKey = ' ' . strtolower($this->groupOperator) . ' ';
    $replacement = '/rows/row[' . implode($groupKey, $filterXpath) . ']';

    $template =
      file_get_contents($this->getXsltTemplatePath('filter.xsl'));
    $this->xslFilters =
      str_replace('__filter_sql__', $replacement, $template);
    $this->xslFilterTransform = $this->getProcessor($this->xslFilters);
  }

  /**
   * Get XSLT template file path.
   *
   * @param string $fileName
   *   The XSLT template filename.
   *
   * @return string
   *   The real path to file.
   */
  protected function getXsltTemplatePath($fileName) {
    $path = Module::getHandler('web_service_client_soap')->getPath();
    return $path . '/xsl-templates/' . $fileName;
  }

  /**
   * Apply XSLT template to filter XML result.
   */
  public function applyXslContextualFilterTransform() {
    $groupFilter = [];

    foreach ($this->contextualFilterXpath as $filter_id => $filter) {
      $groupFilter[] = $filter['xpath'];
    }

    $groupKey = ' and ';
    $replacement = '/rows/row[' . implode($groupKey, $groupFilter) . ']';

    $template =
      file_get_contents($this->getXsltTemplatePath('filter.xsl'));
    $this->xslContextualFilters =
      str_replace('__filter_sql__', $replacement, $template);
    $this->xslContextualFilterTransform = $this->getProcessor($this->xslContextualFilters);
  }

  /**
   * Apply XSLT template for sorted columns.
   */
  public function applyXslSortTransform() {
    $replacement = implode('', $this->xslSorts);
    $template = file_get_contents($this->getXsltTemplatePath('sort.xsl'));
    $pattern = str_replace('__sort_pattern__', $replacement, $template);
    $this->xslSorts = $pattern;
    $this->xslSortTransform = $this->getProcessor($pattern);
    //    foreach ($this->xslSorts as $id => $template) {
    //      $this->xslSortTransform = $this->getProcessor($template);
    //    }
  }

  /**
   * Get XSLT template for base_table.
   *
   * @return string
   *   The XSLT template from the views data definition.
   */
  public function getBaseTableXsltTemplate() {
    $base_table = $this->view->storage->get('base_table');
    $data = Views::viewsData()->getAll();

    return $data[$base_table]['table']['base']['web_service_client_extra']['xsl'];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    // When creating a new view, there won't be a query set yet.
    if (empty($this->operationArguments)) {
      $this->messenger->setMessage(
        $this->t('Please configure the query settings.'),
        'warning'
      );
      return;
    }

    $start = microtime(TRUE);

    try {
      libxml_clear_errors();
      $use_errors = libxml_use_internal_errors(TRUE);

      $this->doExecute($view);

      if ($this->livePreview && $this->options['show_errors']) {
        foreach (libxml_get_errors() as $error) {
          $type = $error->level === LIBXML_ERR_FATAL ? 'error' : 'warning';
          $args = [
            '%error' => trim($error->message),
            '%num' => $error->line,
            '%code' => $error->code,
          ];

          $this->messenger->setMessage(
            $this->t('%error on line %num. Error code: %code', $args),
            $type
          );
        }
      }

      libxml_use_internal_errors($use_errors);
      libxml_clear_errors();
    } catch (Exception $e) {
      watchdog_exception('web_service_client_soap', $e);
      $this->messenger->setMessage($this->t('An error occurred while executing the view. Review the system logs for more information.'), 'error');
    }

    $view->execute_time = microtime(TRUE) - $start;
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(ViewExecutable $view) {
    // We don't need to store the SOAP result only the XML.
    $this->endpoint->call($this->operation, [$this->operationArguments], TRUE);
    $xml_result = $this->endpoint->getClient()->__getLastResponse();

    $xml = new DOMDocument();
    $xml->loadXML($xml_result);

    if ($this->xslBaseTableTransform instanceof XSLTProcessor) {
      $xml = $this->xslBaseTableTransform->transformToDoc($xml);
    }

    $global_xpath_value = NULL;
    if (!empty($this->options['global_xpath']) && $xml instanceof DOMDocument) {
      $xml_global_xpath = $xml->saveXML();
      $gxpath = $this->getXpath($xml_global_xpath);
      foreach ($gxpath->query('/rows') as $r) {
        $global_xpath_value = $this->executeRowQuery($gxpath, $this->options['global_xpath'], $r);
      }
    }

    if (!empty($this->xslRelationshipTransform)) {
      foreach ($this->xslRelationshipTransform as $relationship) {
        if ($relationship instanceof XSLTProcessor) {
          $xml = $relationship->transformToDoc($xml);
        }
      }
    }

    if ($this->xslFilterTransform instanceof XSLTProcessor) {
      $xml = $this->xslFilterTransform->transformToDoc($xml);
    }

    if ($this->xslContextualFilterTransform instanceof XSLTProcessor) {
      $xml = $this->xslContextualFilterTransform->transformToDoc($xml);
    }

    if ($this->xslSortTransform instanceof XSLTProcessor) {
      $xml = $this->xslSortTransform->transformToDoc($xml);
    }

    if ($xml instanceof DOMDocument) {
      $xml_string = $xml->saveXML();
      $xpath = $this->getXpath($xml_string);
    }
    else {
      throw new Exception("Error processing xml with xls transformations.");
    }

    $rows = $xml->getElementsByTagName('row');
    foreach ($xpath->query('/rows/row') as $row) {
      $r = clone $row;
      $result_row = new SoapResultRow($this->getBaseTableInfoTemplate(), $r, $this->service->id());
      if (!empty($global_xpath_value)) {
        $result_row->global_value = $global_xpath_value;
      }

      $view->result[] = $result_row;

      $fields = [];
      foreach ($row->childNodes as $field) {
        if ($field->nodeType === XML_ELEMENT_NODE && !$this->hasChild($field)) {
          $fields[$field->nodeName][] = $field->nodeValue;
        }
      }

      foreach ($view->field as $field_name => $field) {
        $realField = $field->realField;
        if (!empty($fields[$realField])) {
          $result_row->$field_name = $fields[$realField];
        }

        if (!empty($field->options['xpath_selector'])) {
          $row_query = $this->executeRowQuery($xpath, $field->options['xpath_selector'], $row);
          $result_row->$field_name = $row_query;
        }
      }

      // Views XML Backend default sorting.
      //      foreach ($this->extraFields as $field_name => $selector) {
      //        $result_row->$field_name = $this->executeRowQuery($xpath, $selector, $row);
      //      }
    }

    if ($view->pager->useCountQuery() || !empty($view->get_total_rows)) {
      // Normall we would call $view->pager->executeCountQuery($count_query);
      // but we can't in this case, so do the calculation ourselves.
      $view->pager->total_items = $rows->length;
      $view->pager->total_items -= $view->pager->getOffset();
    }

    $this->executeSorts($view);

    if (!empty($this->limit) || !empty($this->offset)) {
      // @todo Re-implement the performance optimization. For the case with no
      // sorts, we can avoid parsing the whole file.
      $view->result = array_slice($view->result, (int) $this->offset, (int) $this->limit);
    }

    // Set the index values after all manipulation is done.
    $this->reIndexResults($view);

    $view->pager->postExecute($view->result);
    $view->pager->updatePageInfo();
    $view->total_rows = $view->pager->getTotalItems();
  }

  /**
   * Check if the node has child nodes.
   *
   * @param \DOMNode $p
   *   The node been check.
   *
   * @return bool
   *   Return TRUE if has child nodes.
   */
  public function hasChild(DOMNode $p) {
    if ($p->hasChildNodes()) {
      foreach ($p->childNodes as $c) {
        if ($c->nodeType == XML_ELEMENT_NODE) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Adds a argument to the view.
   *
   * @param string|int $id
   *   The argument ID.
   * @param array $options
   *   Argument definition.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function addArgumentXpath($id, array $options) {
    $this->filterXpath[$id] = $options;

    return $this;
  }

  /**
   * Get the Views filters.
   *
   * @return array
   *   The XPath strings array.
   */
  public function getFilterXpath() {
    return $this->filterXpath;
  }

  /**
   * Set the Views filters.
   *
   * @param array $filterXpath
   *   A XPath string array.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setFilterXpath(array $filterXpath) {
    $this->filterXpath = $filterXpath;
    return $this;
  }

  /**
   * Get the Views filters.
   *
   * @return array
   *   The XPath strings array.
   */
  public function getContextualFilterXpath() {
    return $this->contextualFilterXpath;
  }

  /**
   * Set the Views filters.
   *
   * @param array $contextualFilterXpath
   *   A XPath string array.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setContextualFilterXpath(array $filterXpath) {
    $this->contextualFilterXpath = $filterXpath;
    return $this;
  }

  /**
   * Get the XPath for filtering Views results.
   *
   * @return string
   *   A XPath string.
   */
  public function getXslFilters() {
    return $this->xslFilters;
  }

  /**
   * Set the XPath string to filtering Views results.
   *
   * @param string $xslFilters
   *   An XPath string.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setXslFilters($xslFilters) {
    $this->xslFilters = $xslFilters;

    return $this;
  }

  /**
   * Get the XPath for filtering Views results.
   *
   * @return string
   *   A XPath string.
   */
  public function getXslContextualFilters() {
    return $this->xslContextualFilters;
  }

  /**
   * Get the web service client instance of the View.
   *
   * @return \Drupal\web_service_client\Entity\WsClient
   *   The web service client instance.
   */
  public function getService() {
    return $this->service;
  }

  /**
   * Set a web service client instance for the View.
   *
   * @param \Drupal\web_service_client\Entity\WsClient $service
   *   A web service client instance.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setService(WsClient $service) {
    $this->service = $service;

    return $this;
  }

  /**
   * Get XSLT templates for sorting.
   *
   * @return array
   *   An ordered array of sorting templates.
   */
  public function getXslSorts() {
    return $this->xslSorts;
  }

  /**
   * Set XSLT templates for sorting.
   *
   * @param array $xslSorts
   *   An ordered array of sorting templates.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setXslSorts(array $xslSorts) {
    $this->xslSorts = $xslSorts;
    return $this;
  }

  /**
   * Get the endpoint instance.
   *
   * @return \Drupal\web_service_client_soap\Plugin\WsClient\WsType\WsClientSoapEndPoint
   *   The endpoint for the View.
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   * Set an endpoint instance.
   *
   * @param \Drupal\web_service_client_soap\Plugin\WsClient\WsType\WsClientSoapEndPoint $endpoint
   *   The endpoint instance.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setEndpoint(WsClientSoapEndPoint $endpoint) {
    $this->endpoint = $endpoint;

    return $this;
  }

  /**
   * Get the web service operation.
   *
   * @return string
   *   The operation name.
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Set the web service operation.
   *
   * @param string $operation
   *   The operation name.
   *
   * @return \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setOperation($operation) {
    $this->operation = $operation;

    return $this;
  }

  /**
   * Get OperationArguments.
   *
   * @return array
   *   The OperationArguments.
   */
  public function getOperationArguments() {
    return $this->operationArguments;
  }

  /**
   * Set OperationArguments.
   *
   * @param array $operationArguments
   *   The operationArguments.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function setOperationArguments(array $operationArguments) {
    $this->operationArguments = $operationArguments;

    return $this;
  }

  /**
   * Get XslRelationships.
   *
   * @return array
   *   The XslRelationships.
   */
  public function getXslRelationships() {
    return $this->xslRelationships;
  }

  /**
   * Set XslRelationships.
   *
   * @param array $xslRelationships
   *   The xslRelationships.
   */
  public function setXslRelationships(array $xslRelationships) {
    $this->xslRelationships = $xslRelationships;
  }

  /**
   * Add a sorting column.
   *
   * @param string|int $id
   *   The sorting column.
   * @param array $options
   *   An array of sorting configuration.
   *
   * @return \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function addXslSort($id, array $options) {
    $pattern = <<< XML
<xsl:sort select="__field__" order="__by__" data-type="__datatype__"/>
XML;

    // Set the right data type.
    switch ($options['type']) {
      case 'number':
      case 'numeric':
      case 'integer':
      case 'float':
        $data_type = 'number';
        break;

      default:
        $data_type = 'text';
    }

    $replacement = str_replace(
      ['__field__', '__by__', '__datatype__'],
      [$options['field'], $options['order'], $data_type],
      $pattern
    );

    //    $template = file_get_contents($this->getXsltTemplatePath('sort.xsl'));
    //$this->xslSorts[$id] = str_replace('__sort_pattern__', $replacement, $template);
    $this->xslSorts[$id] = $replacement;

    return $this;
  }

  /**
   * Adds a relationship template.
   *
   * @param string|int $id
   *   An ID for the template.
   * @param array $template
   *   The XSLT template.
   *
   * @return WsClientSoapQueryPlugin
   *   Fluent setter.
   */
  public function addRelationshipXsltTemplate($id, array $template) {
    $this->xslRelationships[$id] = $template;

    return $this;
  }

  /**
   * Initialize web service client metadata.
   */
  public function initializeClientSettings() {
    list($module, $service_name, $operation) =
      explode(':', $this->view->storage->get('base_table'));
    $this->operation = $operation;
    $this->service = WsClient::load($service_name);
    $this->endpoint = $this->service->getEndpoint();
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = QueryPluginBase::defineOptions();

    $options['source']['default'] = 'query';

    $typedData = $this->view->storage->get('base_table');
    list($x, $y, $parameterType) = explode(':', $typedData);
    $data = WsClientDataDefinition::create($typedData);
    $data->setLabel($parameterType);

    $options['arguments']['contains'] =
      WsClientSoapEndPoint::getTypedDataDefinedOptions($data);

    $options['global_xpath']['default'] = '';
    $options['show_errors'] = ['default' => TRUE];

    return $options;
  }

  /**
   * Apply a given XLST template to a XML string.
   *
   * @param array $templates
   *   An array of templates to apply.
   * @param string $xml
   *   The XML string.
   * @param array $parameters
   *   An assoc array of parameters for the XSLT templates (optional).
   *
   * @return string
   *   The transformed XML string.
   */
  private function applyXsltTemplate(array $templates, $xml, array $parameters = []) {
    foreach ($templates as $key => $template) {
      if (!empty($template)) {
        if (is_array($template)) {
          $xml = $this->applyXsltTemplate($template, $xml);
        }
        else {
          $xslt = new XSLTProcessor();
          $xslt->importStylesheet(new SimpleXMLElement($template));

          if (!empty($parameters)) {
            foreach ($parameters as $parameter => $value) {
              $xslt->setParameter('', $parameter, $value);
            }
          }

          $xml = $xslt->transformToXml(new SimpleXMLElement($xml));
          unset($xslt);
        }
      }
    }

    return $xml;
  }

}
