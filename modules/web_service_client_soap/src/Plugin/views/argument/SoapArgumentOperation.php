<?php

namespace Drupal\web_service_client_soap\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\web_service_client_soap\AdminLabelTrait;
use Drupal\web_service_client_soap\ComplexArgument;

/**
 * Argument handler for a operation.
 *
 * @ViewsArgument("web_service_client_soap_contextual_filter_operation_argument")
 */
class SoapArgumentOperation extends ArgumentPluginBase {

  use AdminLabelTrait;

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    //$form['default_argument_skip_url']['#access'] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    $value = $this->getValue();
    if ((!is_object($value) || !($value instanceof ComplexArgument)) && !is_array($value)) {
      if (!empty($value) && is_string($value)) {
        $value = unserialize(base64_decode(urldecode($value)));
      }

      if (!is_array($value)) {
        $value = $this->getDefaultArgument();
      }
      else {
        $value = new ComplexArgument($value);
      }
    }
    $this->query->operationContextualFilter = $value;
  }

  /**
   * Get a default argument, if available.
   */
  public function getDefaultArgument() {
    $arg = parent::getDefaultArgument();
    if (!empty($arg)) {
      return new ComplexArgument($arg);
    }
    return new ComplexArgument([]);
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['default_argument_skip_url'] = ['default' => TRUE];

    return $options;
  }

}
