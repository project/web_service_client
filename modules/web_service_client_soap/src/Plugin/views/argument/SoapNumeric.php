<?php

namespace Drupal\web_service_client_soap\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;
use Drupal\web_service_client_soap\Plugin\views\filter\SoapFilterHelperTrait;

/**
 * Argument handler for a operation.
 *
 * @ViewsArgument("web_service_client_soap_argument_numeric")
 */
class SoapNumeric extends SoapStandard {

  use SoapFilterHelperTrait;
  use AdminLabelTrait;

  public $operator;

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // This is done only for code completion.
    /** @var \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin $query */
    $query = &$this->query;
    $operators = $this->operators();
    $method = $operators[$this->options['operator']]['method'];
    $this->value = $this->getValue();
    $this->operator = $this->options['operator'];
    //$xpath = $this->options['field'] . $this->options['operator'] .'\'' . $this->getValue() . '\'';

    $query->addContextualFilterXpath(
      $this->options['id'],
      [
        'xpath' => call_user_func_array([
          $this,
          $method,
        ], [$this->options['field']]),
      ]);
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['operator'] = ['default' => '='];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['operator'] = [
      '#title' => $this->t('Operator'),
      '#type' => 'select',
      '#options' => $this->fieldsOperatorOptions(),
      '#default_value' => $this->options['operator'],
    ];
  }

  public function opBetween($field) {
    if ($this->operator == 'between') {
      return "(number($field) >= {$this->value['min']} and {$this->value['max']} >= number($field))";
    }
    else {
      return "not((number($field) >= {$this->value['min']} and {$this->value['max']} >= number($field)))";
    }
  }

  function operators() {
    $operators = [
      '<' => [
        'title' => $this->t('Is less than'),
        'method' => 'opSimple',
        'short' => $this->t('<'),
        'values' => 1,
      ],
      '<=' => [
        'title' => $this->t('Is less than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('<='),
        'values' => 1,
      ],
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'opSimple',
        'short' => $this->t('='),
        'values' => 1,
      ],
      '!=' => [
        'title' => $this->t('Is not equal to'),
        'method' => 'opSimple',
        'short' => $this->t('!='),
        'values' => 1,
      ],
      '>=' => [
        'title' => $this->t('Is greater than or equal to'),
        'method' => 'opSimple',
        'short' => $this->t('>='),
        'values' => 1,
      ],
      '>' => [
        'title' => $this->t('Is greater than'),
        'method' => 'opSimple',
        'short' => $this->t('>'),
        'values' => 1,
      ],
      'between' => [
        'title' => $this->t('Is between'),
        'method' => 'opBetween',
        'short' => $this->t('between'),
        'values' => 2,
      ],
      'not between' => [
        'title' => $this->t('Is not between'),
        'method' => 'opBetween',
        'short' => $this->t('not between'),
        'values' => 2,
      ],
    ];

    // If the definition allows for the empty operator, add it.
    if (!empty($this->definition['allow empty'])) {
      $operators += [
        'empty' => [
          'title' => $this->t('Is empty (NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('empty'),
          'values' => 0,
        ],
        'not empty' => [
          'title' => $this->t('Is not empty (NOT NULL)'),
          'method' => 'opEmpty',
          'short' => $this->t('not empty'),
          'values' => 0,
        ],
      ];
    }

    return $operators;
  }

  public function opSimple($field) {
    if (is_array($this->value)) {
      $values = $this->value;
    }
    else {
      $values = explode(',', $this->value);
      if (count($values) == 1) {
        $values = explode('+', $this->value);
      }
    }
    $result = [];
    $operators = $this->operators();
    $operator = $operators[$this->operator];
    foreach ($values as $value) {
      switch ($this->operator) {
        case '<':
          $result[] = "{$value} > number($field)";
          break;

        case '<=':
          $result[] = "{$value} >= number($field)";
          break;

        default:
          $result[] = "number($field) " . $this->operator . " {$value}";
      }
    }
    return '(' . implode(' ' . !empty($operator['logic operator']) ? $operator['logic operator'] : 'and' . ' ', $result) . ')';
  }

}
