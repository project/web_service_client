<?php

namespace Drupal\web_service_client_soap\Plugin\views\argument;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Drupal\web_service_client_soap\AdminLabelTrait;
use Drupal\web_service_client_soap\Plugin\views\filter\SoapFilterHelperTrait;

/**
 * Argument handler for a operation.
 *
 * @ViewsArgument("web_service_client_soap_argument_standard")
 */
class SoapStandard extends ArgumentPluginBase {

  use SoapFilterHelperTrait;
  use AdminLabelTrait;

  public $operator;

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // This is done only for code completion.
    /** @var \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin $query */
    $query = &$this->query;
    $operators = $this->operators();
    $method = $operators[$this->options['operator']]['method'];
    $this->value = $this->getValue();
    $this->operator = $this->options['operator'];
    //$xpath = $this->options['field'] . $this->options['operator'] .'\'' . $this->getValue() . '\'';

    $query->addContextualFilterXpath(
      $this->options['id'],
      [
        'xpath' => call_user_func_array([
          $this,
          $method,
        ], [$this->options['field']]),
      ]);
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['operator'] = ['default' => '='];

    return $options;
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['operator'] = [
      '#title' => $this->t('Operator'),
      '#type' => 'select',
      '#options' => $this->fieldsOperatorOptions(),
      '#default_value' => $this->options['operator'],
    ];

  }

  /**
   * Provide a list of all operators.
   */
  function fieldsOperatorOptions() {
    $operators = [];
    foreach ($this->operators() as $key => $value) {
      $operators[$key] = $value['title'];
    }
    return $operators;
  }

}
