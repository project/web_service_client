<?php

namespace Drupal\web_service_client_soap\Plugin\views\field;

use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * A handler to provide an XML markup field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("web_service_client_soap_field_numeric")
 */
class SoapNumeric extends NumericField {

  use SoapFieldHelperTrait;
  use AdminLabelTrait;

  /**
   * Render all items in this field together.
   *
   * @param array $items
   *   The items provided by getItems for a single row.
   *
   * @return string
   *   The rendered items.
   *
   * @see \Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface
   */
  public function render(ResultRow $values) {
    $items = $this->getItems($values);
    if (!empty($items)) {
      if ($this->options['type'] == 'separator') {
        $render = [
          '#type' => 'inline_template',
          '#template' => '{{ items|safe_join(separator) }}',
          '#context' => [
            'items' => $items,
            'separator' => $this->sanitizeValue($this->options['separator'], 'xss_admin'),
          ],
        ];
      }
      else {
        $render = [
          '#theme' => 'item_list',
          '#items' => $items,
          '#title' => NULL,
          '#list_type' => $this->options['type'],
        ];
      }
      return drupal_render($render);
    }
  }

}
