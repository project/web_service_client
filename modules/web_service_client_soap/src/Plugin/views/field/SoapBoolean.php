<?php

namespace Drupal\web_service_client_soap\Plugin\views\field;

use Drupal\views\Plugin\views\field\Boolean;
use Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface;

/**
 * A handler to provide an XML field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("web_service_client_soap_field_boolean")
 */
class SoapBoolean extends Boolean implements MultiItemsFieldHandlerInterface {

  // use XmlFieldHelperTrait;
  use SoapFieldHelperTrait;
}
