<?php

namespace Drupal\web_service_client_soap\Plugin\views\field;

use Drupal\views\Plugin\views\field\Date as ViewsDate;
use Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface;
use Drupal\views\ResultRow;
use Drupal\views_xml_backend\Sorter\DateSorter;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * A handler to provide an XML date field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("web_service_client_soap_field_datetime")
 */
class SoapDate extends ViewsDate implements MultiItemsFieldHandlerInterface {

  use SoapFieldHelperTrait;
  use AdminLabelTrait;

  /**
   * {@inheritdoc}
   */
  public function render_item($count, $item) {
    $tmp_row = new ResultRow();

    if (!is_numeric($item['value'])) {
      $tmp_row->{$this->field_alias} = strtotime($item['value']);
    }
    else {
      $tmp_row->{$this->field_alias} = $item['value'];
    }

    return parent::render($tmp_row);
  }

  /**
   * {@inheritdoc}
   */
  public function clickSort($order) {
    $this->query->addSort(new DateSorter($this->field_alias, $order));
  }

}
