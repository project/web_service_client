<?php

/**
 * @file
 * Contains \Drupal\web_service_client_soap\Plugin\views\field\SoapStandard.
 */

namespace Drupal\web_service_client_soap\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * A handler to provide an XML text field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("web_service_client_soap_field_string")
 */
class SoapString extends FieldPluginBase implements MultiItemsFieldHandlerInterface {

  use SoapFieldHelperTrait;
  use AdminLabelTrait;
}
