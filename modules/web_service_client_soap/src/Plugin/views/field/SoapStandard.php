<?php

namespace Drupal\web_service_client_soap\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * A handler to provide an XML text field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("web_service_client_soap_field_standard")
 */
class SoapStandard extends FieldPluginBase implements MultiItemsFieldHandlerInterface {

  use SoapFieldHelperTrait;
  use AdminLabelTrait;
}
