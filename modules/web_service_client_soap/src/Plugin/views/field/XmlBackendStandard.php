<?php

/**
 * @file
 * Contains \Drupal\views_xml_backend\Plugin\views\field\Standard.
 */

namespace Drupal\web_service_client_soap\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface;
use Drupal\views_xml_backend\Plugin\views\field\Standard;

/**
 * A handler to provide an XML text field.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("views_xml_backend_standard")
 */
class XmlBackendStandard extends Standard implements MultiItemsFieldHandlerInterface {

  /**
   * Returns the default options for XML fields.
   *
   * @return array
   *   The default options array.
   */
  protected function getDefaultXmlOptions() {
    $options = parent::getDefaultXmlOptions();

    $options['ul_prefix']['default'] = '';
    $options['ul_suffix']['default'] = '';

    return $options;
  }

  /**
   * Returns the default options form for XML fields.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form array.
   */
  public function getDefaultXmlOptionsForm(array $form, FormStateInterface $form_state) {
    $form = parent::getDefaultXmlOptionsForm($form, $form_state);

    $form['type']['#options']['ul_prefix_suffix'] = $this->t('Unordered list with prefix and suffix');

    $form['ul_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#default_value' => $this->options['ul_prefix'],
      '#states' => [
        'visible' => [
          ':input[name="options[type]"]' => ['value' => 'ul_prefix_suffix'],
        ],
      ],
    ];

    $form['ul_suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix'),
      '#default_value' => $this->options['ul_suffix'],
      '#states' => [
        'visible' => [
          ':input[name="options[type]"]' => ['value' => 'ul_prefix_suffix'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Render all items in this field together.
   *
   * @param array $items
   *   The items provided by getItems for a single row.
   *
   * @return string
   *   The rendered items.
   *
   * @see \Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface
   */
  public function renderItems($items) {
    if (!empty($items)) {
      if ($this->options['type'] == 'ul_prefix_suffix') {
        foreach ($items as $key => $item) {
          $items[$key] = [
            '#type' => 'container',
            'prefix' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#attributes' => ['class' => ['prefix-' . $key]],
              '#value' => $this->t($this->options['ul_prefix']),
            ],
            'item' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => $item,
            ],
            'suffix' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#attributes' => ['class' => ['suffix-' . $key]],
              '#value' => $this->t($this->options['ul_suffix']),
            ],
          ];
        }

        $render = [
          '#theme' => 'item_list',
          '#items' => $items,
          '#title' => NULL,
          '#list_type' => $this->options['type'],
        ];
      }
      else {
        return parent::renderItems($items);
      }
      return drupal_render($render);
    }
  }

}
