<?php

namespace Drupal\web_service_client_soap\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\web_service_client_soap\AdminLabelTrait;

/**
 * Numeric sort plugin for XML.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("web_service_client_soap_sort_numeric")
 */
class SoapNumeric extends SortPluginBase {

  use SoapSortHelperTrait;
  use AdminLabelTrait;
}
