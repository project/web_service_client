<?php

namespace Drupal\web_service_client_soap\Plugin\views\sort;

/**
 * A handler to provide an XML text field.
 */
trait SoapSortHelperTrait {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This is done only for code completion.
    /** @var \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin $query */
    $query = &$this->query;
    $order = ($this->options['order'] == 'ASC') ? 'ascending' : 'descending';

    $arr = explode('_', $this->options['plugin_id']);
    $arr = array_combine($arr, $arr);

    $query->addXslSort(
      $this->options['id'],
      [
        'field' => $this->options['field'],
        'order' => $order,
        'type' => !empty($arr['numeric']) ? 'numeric' : 'text',
      ]
    );
  }

}
