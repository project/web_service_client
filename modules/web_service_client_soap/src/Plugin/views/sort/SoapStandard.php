<?php

namespace Drupal\web_service_client_soap\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\web_service_client_soap\AdminLabelTrait;
use Drupal\web_service_client_soap\Plugin\views\filter\SoapSortHelperTrait;

/**
 * Default implementation of the base sort plugin.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("web_service_client_soap_sort_standard")
 */
class SoapStandard extends SortPluginBase {

  use SoapSortHelperTrait;
  use AdminLabelTrait;
}
