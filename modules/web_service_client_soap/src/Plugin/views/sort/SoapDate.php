<?php

namespace Drupal\web_service_client_soap\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Date;
use Drupal\web_service_client_soap\AdminLabelTrait;
use Drupal\web_service_client_soap\Plugin\views\filter\SoapSortHelperTrait;

/**
 * Date sort plugin for views_xml_backend.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("web_service_client_soap_sort_datetime")
 */
class SoapDate extends Date {

  use SoapSortHelperTrait;
  use AdminLabelTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $options = $this->options;
    $id = $options['id'];
    $field = $options['field'];
    $order = $options['order'] == 'ASC' ? 'ascending' : 'descending';

    $this->query->xslSorts[$id] = [
      'field' => "number(translate(translate($field, '-', ''), ':', ''))",
      'order' => $order,
      'type' => 'numeric',
    ];
  }

}
