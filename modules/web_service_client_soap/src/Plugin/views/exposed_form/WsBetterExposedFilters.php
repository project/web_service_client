<?php

namespace Drupal\web_service_client_soap\Plugin\views\exposed_form;

use Drupal\better_exposed_filters\Plugin\views\exposed_form\BetterExposedFilters;

/**
 * Exposed form plugin that provides a basic exposed form.
 *
 * @ingroup views_exposed_form_plugins
 *
 * @ViewsExposedForm(
 *   id = "wsbef",
 *   title = @Translation("Webservice Better Exposed Filters"),
 *   help = @Translation("Provides additional options for exposed form
 *   elements. Order by filter")
 * )
 */
class WsBetterExposedFilters extends BetterExposedFilters {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $view = $this->view;
    $exposed_data = isset($view->exposed_data) ? $view->exposed_data : [];
    $sort_by = isset($exposed_data['sort_by']) ? $exposed_data['sort_by'] : NULL;
    if (!empty($sort_by)) {

      $arr = $view->query->getXslSorts();
      $value = $arr[$sort_by];
      unset($arr[$sort_by]);
      $arr = array_merge([$sort_by => $value], $arr);
      $view->query->setXslSorts($arr);

      // Make sure the original order of sorts is preserved
      // (e.g. a sticky sort is often first)
      if (isset($view->sort[$sort_by])) {
        $view->query->orderby = [];
        foreach ($view->sort as $key => $sort) {
          if (!$sort->isExposed()) {
            $sort->query();
          }
          elseif ($key == $sort_by) {
            if (isset($exposed_data['sort_order']) && in_array($exposed_data['sort_order'], [
                'ASC',
                'DESC',
              ])) {
              $sort->options['order'] = $exposed_data['sort_order'];
            }
            $sort->setRelationship();
            $sort->query();
          }
        }
      }
    }
  }

}
