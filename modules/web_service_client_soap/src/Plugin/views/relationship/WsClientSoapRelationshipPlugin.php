<?php

namespace Drupal\web_service_client_soap\Plugin\views\relationship;

use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;

/**
 * Denormalize SOAP complex types as Views relationships.
 *
 * @ingroup views_relationship_handlers
 *
 * @ViewsRelationship("web_service_client_soap_relationship")
 */
class WsClientSoapRelationshipPlugin extends RelationshipPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This is done only for code completion.
    /** @var \Drupal\web_service_client_soap\Plugin\views\query\WsClientSoapQueryPlugin $query */
    $query = &$this->query;

    $query->addRelationshipXsltTemplate(
      $this->options['id'],
      [
        'parent' => $this->options['relationship'],
        'xsl' => $this->configuration['web_service_client_extra']['xsl'],
        'name' => $this->configuration['web_service_client_extra']['result_name'],
        'type' => $this->configuration['web_service_client_extra']['result_type'],
      ]
    );
  }

}
