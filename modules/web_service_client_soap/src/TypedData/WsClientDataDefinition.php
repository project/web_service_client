<?php

namespace Drupal\web_service_client_soap\TypedData;

use DOMElement;
use Drupal;
use Drupal\web_service_client\TypedData\WsClientDataDefinition as ParentWsClientDataDefinition;
use Drupal\web_service_client\TypedData\WsClientListDataDefinition;

/**
 * {@inheritdoc}
 */
class WsClientDataDefinition extends ParentWsClientDataDefinition {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinition($name, $xml = NULL) {
    if ($xml === NULL || !$xml instanceof DOMElement) {
      return parent::getPropertyDefinition($name);
    }
    $xsi_type = $xml->getAttribute('xsi:type');
    if (!empty($xsi_type)) {
      list($namespace, $type) = explode(':', $xsi_type, 2);
    }
    else {
      return parent::getPropertyDefinition($name);
    }
    $definition = parent::getPropertyDefinition($name);
    $data_type = $this->definition['type'];
    $data_type_parts = explode(':', $data_type);
    $data_type_parts[2] = $type;
    if ($definition instanceof WsClientListDataDefinition) {
      $definition = Drupal::service('web_service_client_soap.typed_data_manager')
        ->createListDataDefinition(implode(':', $data_type_parts));
    }
    else {
      $definition = Drupal::service('web_service_client_soap.typed_data_manager')
        ->createDataDefinition(implode(':', $data_type_parts));
    }
    return $definition;
  }

}
