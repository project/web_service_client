<?php

namespace Drupal\web_service_client_soap\TypedData;

use Drupal\web_service_client\TypedData\WsClientListDataDefinition as ParentWsClientListDataDefinition;

/**
 * A typed data definition class for defining WsClientData lists.
 */
class WsClientListDataDefinition extends ParentWsClientListDataDefinition {

}
