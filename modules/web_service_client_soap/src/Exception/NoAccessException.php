<?php

namespace Drupal\web_service_client_soap\Exception;

use Throwable;

/**
 * Thrown when request authentication data is invalid or missing.
 *
 * @package Drupal\web_service_client_soap\Exception
 */
class NoAccessException extends Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(Throwable $soapFault) {
    parent::__construct(t("Bad credentials or access denied error: @error.", [
      '@error' => $soapFault->getMessage(),
    ]), 403, $soapFault);
  }

}
