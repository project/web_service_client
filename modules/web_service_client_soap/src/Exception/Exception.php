<?php

namespace Drupal\web_service_client_soap\Exception;

use RuntimeException;

/**
 * Generic exception.
 *
 * @package Drupal\web_service_client_soap\Exception
 */
class Exception extends RuntimeException {

}
