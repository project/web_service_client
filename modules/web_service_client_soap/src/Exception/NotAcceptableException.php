<?php

namespace Drupal\web_service_client_soap\Exception;

use Throwable;

/**
 * Thrown when the server cannot parse the request due invalid data format.
 *
 * @package Drupal\web_service_client_soap\Exception
 */
class NotAcceptableException extends Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(Throwable $soapFault) {
    parent::__construct(t("The request contains bogus or corrupt data: @error.", [
      '@error' => $soapFault->getMessage(),
    ]), 406, $soapFault);
  }

}
