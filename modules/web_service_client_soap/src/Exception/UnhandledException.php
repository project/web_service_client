<?php

namespace Drupal\web_service_client_soap\Exception;

use Throwable;

/**
 * Thrown when the exception code is unknown.
 *
 * @package Drupal\web_service_client_soap\Exception
 */
class UnhandledException extends Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(Throwable $soapFault) {
    parent::__construct(t("An unexpected error has occurred: @error.", [
      '@error' => $soapFault->getMessage(),
    ]), 500, $soapFault);
  }

}
