<?php

namespace Drupal\web_service_client_soap\Exception;

use Throwable;

/**
 * Thrown when the requested resource is unavailable.
 *
 * @package Drupal\web_service_client_soap\Exception
 */
class GoneException extends Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(Throwable $soapFault) {
    parent::__construct(t("The request contains a resource that is not longer available: @error.", [
      '@error' => $soapFault->getMessage(),
    ]), 410, $soapFault);
  }

}
