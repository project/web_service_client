<?php

namespace Drupal\web_service_client_soap\Exception;

use Throwable;

/**
 * Thrown when the request parameters are invalid.
 *
 * @package Drupal\web_service_client_soap\Exception
 */
class BadRequestException extends Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(Throwable $soapFault) {
    parent::__construct(t("Parameters may be invalid or missing: @error.", [
      '@error' => $soapFault->getMessage(),
    ]), 400, $soapFault);
  }

}
