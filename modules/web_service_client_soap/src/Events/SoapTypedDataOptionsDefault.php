<?php

namespace Drupal\web_service_client_soap\Events;

use Drupal\Core\TypedData\DataDefinition;
use Symfony\Component\EventDispatcher\Event;

/**
 * Collects ajax commands and return values on gadget popup callback.
 */
class SoapTypedDataOptionsDefault extends Event {

  protected $defaults;

  protected $dataType;

  protected $context;

  public function __construct($defaults, DataDefinition $dataType, $context) {
    $this->defaults = $defaults;
    $this->dataType = $dataType;
    $this->context = $context;
  }

  public function getDefaults() {
    return $this->defaults;
  }

}
