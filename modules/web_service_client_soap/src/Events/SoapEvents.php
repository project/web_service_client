<?php

namespace Drupal\web_service_client_soap\Events;

/**
 * Contains all events thrown by entity browser.
 */
final class SoapEvents {

  /**
   * The typedData form.
   *
   * @var string
   */
  const CUSTOMIZE_FORM_ELEMENTS = 'web_service_client_soap.customize_form_elements';

  /**
   * The typedData options default.
   *
   * @var string
   */
  const CUSTOMIZE_OPTIONS_DEFAULT = 'web_service_client_soap.customize_options_default';

}
