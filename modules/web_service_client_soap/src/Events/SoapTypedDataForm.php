<?php

namespace Drupal\web_service_client_soap\Events;

use Drupal\Core\TypedData\DataDefinition;
use Symfony\Component\EventDispatcher\Event;

/**
 * Collects ajax commands and return values on gadget popup callback.
 */
class SoapTypedDataForm extends Event {

  protected $form;

  protected $dataType;

  protected $defaultValues;

  protected $context;

  /**
   * SoapTypedDataForm constructor.
   *
   * {@inheritdoc}
   */
  public function __construct($form, DataDefinition $dataType, $default_values, $context) {
    $this->form = $form;
    $this->dataType = $dataType;
    $this->defaultValues = $default_values;
    $this->context = $context;
  }

  /**
   * Get the form.
   *
   * @return mixed
   *   The form related to the event.
   */
  public function getForm() {
    return $this->form;
  }

  public function setForm($form) {
    $this->form = $form;
  }

  /**
   * @return \Drupal\Core\TypedData\DataDefinition
   */
  public function getDataType() {
    return $this->dataType;
  }

  /**
   * @param \Drupal\Core\TypedData\DataDefinition $dataType
   *
   * @return SoapTypedDataForm
   */
  public function setDataType($dataType) {
    $this->dataType = $dataType;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getDefaultValues() {
    return $this->defaultValues;
  }

  /**
   * @param mixed $defaultValues
   *
   * @return SoapTypedDataForm
   */
  public function setDefaultValues($defaultValues) {
    $this->defaultValues = $defaultValues;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * @param mixed $context
   *
   * @return SoapTypedDataForm
   */
  public function setContext($context) {
    $this->context = $context;
    return $this;
  }

}
