<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="no"/>

  <xsl:param name="parent-data-type" select="null"/>
  <xsl:param name="parent-name" select="null"/>
  <xsl:param name="parent-trail" select="null"/>

  <xsl:attribute-set name="data-type-info">
    <xsl:attribute name="parent-data-type">
      <xsl:value-of select="$parent-data-type"/>
    </xsl:attribute>
    <xsl:attribute name="parent-name">
      <xsl:value-of select="$parent-name"/>
    </xsl:attribute>
    <xsl:attribute name="parent-trail">
      <xsl:value-of select="$parent-trail"/>
    </xsl:attribute>
  </xsl:attribute-set>

  <xsl:template match="/">
    <rows>
      <xsl:apply-templates select="/rows/row/__table_key__"/>
    </rows>
  </xsl:template>

  <xsl:template match="/rows/row/__table_key__">
    <row>
      <xsl:for-each select="preceding-sibling::node()">
        <xsl:if test="name() != '__table_key__'">
          <xsl:copy-of select="."/>
        </xsl:if>
      </xsl:for-each>
      <xsl:apply-templates select="child::node()"/>
      <xsl:for-each select="following-sibling::node()">
        <xsl:if test="name() != '__table_key__'">
          <xsl:copy-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </row>
  </xsl:template>

  <xsl:template match="child::node()">
    <xsl:if test="@*">
      <xsl:copy>
        <xsl:for-each select="@*">
          <xsl:variable name="attributeName" select="local-name()"/>
          <xsl:attribute name="{$attributeName}">
            <xsl:value-of select="."/>
          </xsl:attribute>
        </xsl:for-each>
        <xsl:attribute name="parent-data-type">
          <xsl:value-of select="$parent-data-type"/>
        </xsl:attribute>
        <xsl:attribute name="parent-name">
          <xsl:value-of select="$parent-name"/>
        </xsl:attribute>
        <xsl:attribute name="parent-trail">
          <xsl:value-of select="$parent-trail"/>
        </xsl:attribute>
        <xsl:for-each select="child::node()">
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </xsl:copy>
    </xsl:if>
    <xsl:if test="not(@*)">
      <xsl:copy use-attribute-sets="data-type-info">
        <xsl:for-each select="child::node()">
          <xsl:copy-of select="."/>
        </xsl:for-each>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
