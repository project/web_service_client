<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes"/>

  <xsl:template match="/">
    <rows>
      <xsl:for-each select="rows/row[(finalPrice='123' and selected='true') or (hasPrice='789')]">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </rows>
  </xsl:template>

</xsl:stylesheet>
