<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
  <xsl:output indent="yes"/>

  <xsl:template match="*[local-name() = 'getPotentialPackagesResponse']">
    <rows>
      <xsl:apply-templates select="packageAvailabilityResponse"/>
    </rows>
  </xsl:template>

  <xsl:template match="packageAvailabilityResponse">
    <row>
      <xsl:for-each select="child::node()">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </row>
  </xsl:template>

</xsl:stylesheet>
