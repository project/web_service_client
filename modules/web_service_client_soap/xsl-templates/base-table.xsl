<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
  <xsl:output indent="no"/>

  <xsl:param name="parent-data-type" select="null"/>
  <xsl:param name="parent-name" select="null"/>
  <xsl:param name="parent-trail" select="null"/>

  <xsl:attribute-set name="data-type-info">
    <xsl:attribute name="parent-data-type">
      <xsl:value-of select="$parent-data-type"/>
    </xsl:attribute>
    <xsl:attribute name="parent-name">
      <xsl:value-of select="$parent-name"/>
    </xsl:attribute>
    <xsl:attribute name="parent-trail">
      <xsl:value-of select="$parent-trail"/>
    </xsl:attribute>
  </xsl:attribute-set>

  <xsl:template match="*[local-name() = '__base_result_type__']">
    <rows>
      <xsl:apply-templates select="__table_key__"/>
    </rows>
  </xsl:template>

  <xsl:template match="__table_key__">
    <row>
      <xsl:for-each select="child::node()">
        <xsl:copy use-attribute-sets="data-type-info">
          <xsl:for-each select="child::node()">
            <xsl:copy-of select="."/>
          </xsl:for-each>
        </xsl:copy>
      </xsl:for-each>
    </row>
  </xsl:template>

</xsl:stylesheet>
