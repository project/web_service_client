<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="yes"/>

  <xsl:template match="/">
    <rows>
      <xsl:apply-templates select="/rows/row/productResult"/>
    </rows>
  </xsl:template>

  <xsl:template match="/rows/row/productResult">
    <row>
      <xsl:for-each select="preceding-sibling::node()">
        <xsl:if test="name() != 'productResult'">
          <xsl:copy-of select="."/>
        </xsl:if>
      </xsl:for-each>
      <xsl:for-each select="child::node()">
        <xsl:copy-of select="."/>
      </xsl:for-each>
      <xsl:for-each select="following-sibling::node()">
        <xsl:if test="name() != 'productResult'">
          <xsl:copy-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </row>
  </xsl:template>

</xsl:stylesheet>
