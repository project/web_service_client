<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output indent="no"/>

  <xsl:template match="*/row">
    <rows>
      <xsl:apply-templates select="row"/>
    </rows>
  </xsl:template>

  <xsl:template match="row">
    <row>
      <xsl:for-each select="child::node()">
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </row>
  </xsl:template>

</xsl:stylesheet>
