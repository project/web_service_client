<?php

/**
 * Implements hook_views_data_alter().
 */
function web_service_client_soap_views_data_alter(array &$data) {
  foreach ($data as $table => $info) {
    if (!empty($info['table']['base']['web_service_client_extra']['plugin_id']) && $info['table']['base']['web_service_client_extra']['plugin_id'] == 'soap') {
      $data[$table] += get_xml_backend_generic_fields();
    }
  }
}

/**
 * Get generic fields from views_xml_backend module.
 *
 * @return mixed
 *   The fields
 */
function get_xml_backend_generic_fields() {
  $fields = views_xml_backend_views_data();
  unset($fields['views_xml_backend']['table']);

  $return = [];
  foreach ($fields['views_xml_backend'] as $key => $value) {
    $value['group'] = 'XML';
    $return['xml_back_' . $key] = $value;
  }

  return $return;
}

/**
 * Implements hook_views_plugins_field_alter().
 */
function web_service_client_soap_views_plugins_field_alter(array &$plugins) {
  $class = 'Drupal\\web_service_client_soap\\Plugin\\views\\field\\XmlBackendStandard';
  $plugins['views_xml_backend_standard']['class'] = $class;
}
