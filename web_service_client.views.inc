<?php

/**
 * @file
 * Contains web_service_client.views.inc.
 */

use Drupal\web_service_client\Entity\WsClient;

/**
 * Implements hook_views_data().
 */
function web_service_client_views_data() {
  $data = [];

  $services = WsClient::loadMultiple();

  $primitive_types = web_service_client_data_types();
  $primitive_types = $primitive_types['Others'];

  foreach ($services as $name => $service) {
    $operations = $service->getOperations();
    $data_types = $service->getDataTypes();

    $types = Drupal::service('plugin.manager.web_service_client.type');
    $endpoint = $service->getEndpoint();

    if (!empty($operations)) {
      _web_service_client_views_data_process_operations($data, $service, $operations, $data_types, $primitive_types, $endpoint);
    }

    if (!empty($data_types)) {
      _web_service_client_views_data_process_data_types($data, $service, $operations, $data_types, $primitive_types, $endpoint);
    }
  }

  return $data;
}

/**
 * Expose data types as tables for relationships.
 *
 * @param $data
 * @param $service
 * @param $operations
 * @param $data_types
 * @param $primitive_types
 * @param $endpoint
 */
function _web_service_client_views_data_process_operations(&$data, $service, $operations, $data_types, $primitive_types, $endpoint) {
  $plugin_definition = $endpoint->getPluginDefinition();

  foreach ($operations as $op_name => $operation) {
    if (empty($operation['views_settings']['availability'])) {
      continue;
    }
    $table_key = 'web_service_client:' . $service->id() . ':' . $op_name;
    $table_label = 'WS Client: ' . $service->label() . ' -> ' . $operation['label'];
    $query_id = $plugin_definition['views']['query_plugin'];
    $result_type = $operation['result']['type'];
    $properties = $data_types[$result_type]['property info'];

    $data[$table_key]['table']['group'] = $table_label;
    $data[$table_key]['table']['base'] = [
      'title' => $table_label,
      'help' => t('Queries an XML file.'),
      'query_id' => $query_id,
      'web_service_client_extra' => $endpoint->getDefinitionExtraBase($op_name),
    ];
    $data[$table_key]['table']['base']['web_service_client_extra']['plugin_id'] = $service->getType();

    if (count($properties) == 1) {
      $property = reset($properties);
      if (empty($primitive_types[$property['type']])) {
        $endpoint->dataProcessOperationsAlter($properties, $result_type, $operations, $data_types, $primitive_types);
      }
    }

    foreach ($properties as $prop_key => $property) {
      _web_service_client_build_property_data($data, $prop_key, $property, $primitive_types, $plugin_definition, $service, $endpoint, $table_key);
    }

    // Process the parameters.
    foreach ($operation['parameter'] as $param_name => $param_data) {
      $parameter_type = $param_data['type'];
      $param_type = isset($data_types[$parameter_type]) ? $data_types[$parameter_type] : $primitive_types[$parameter_type];
      $data_definition_type = 'web_service_client:' . $service->id() . ':' . $param_data['type'];
      $filter_id = $plugin_definition['views']['filter_operation_plugin'];

      if (empty($param_type['property info'])) {
        $filter_id = $plugin_definition['views']['filter_plugins'][strtolower($param_type)];
      }

      // Add especial filter that are arguments that use calling the operation.
      $data[$table_key][$parameter_type] = [
        'title' => $parameter_type . ' ' . t('(Operation Parameter)'),
        'help' => $parameter_type . ' ' . t('(Operation Parameter)'),
        'filter' => [
          'id' => $filter_id,
          'web_service_client_extra' => ['typedData' => $data_definition_type],
        ],
        'argument' => [
          'id' => $plugin_definition['views']['argument_operation_plugin'],
          'web_service_client_extra' => ['typedData' => $data_definition_type],
        ],
      ];
    }
  }
}

/**
 * @param $data
 * @param $service
 * @param $operations
 * @param $data_types
 * @param $primitive_types
 * @param $endpoint
 */
function _web_service_client_views_data_process_data_types(&$data, $service, $operations, $data_types, $primitive_types, $endpoint) {
  $plugin_definition = $endpoint->getPluginDefinition();

  foreach ($data_types as $op_name => $operation) {
    $table_key = 'web_service_client:' . $service->id() . ':' . $op_name;
    $table_label = 'WS Client: ' . $service->label() . ' -> ' . $operation['label'];
    $query_id = $plugin_definition['views']['query_plugin'];

    if (!empty($data[$table_key])) {
      continue;
    }

    $data[$table_key]['group'] = $table_label;

    $data[$table_key]['table'] = [
      'title' => $table_label,
      'help' => t('Queries an XML file.'),
      'group' => $table_label,
    ];

    $properties = $operation['property info'];

    foreach ($properties as $key => $property) {
      _web_service_client_build_property_data($data, $key, $property, $primitive_types, $plugin_definition, $service, $endpoint, $table_key);
    }
  }
}

function _web_service_client_build_property_data(&$data, $key, $property, $primitive_types, $plugin_definition, $service, $endpoint, $table_key) {
  $type = $property['type'];
  if (!empty($primitive_types[$type])) {
    $data[$table_key][$key] = [
      'title' => $key,
      'help' => $key,
      'field' => [
        'id' => !empty($plugin_definition['views']['field_plugins'][$type])
          ? $plugin_definition['views']['field_plugins'][$type] : $plugin_definition['views']['field_plugins']['standard'],
      ],
      'argument' => [
        'id' => !empty($plugin_definition['views']['argument_plugins'][$type])
          ? $plugin_definition['views']['argument_plugins'][$type] : $plugin_definition['views']['argument_plugins']['standard'],
      ],
      'filter' => [
        'id' => !empty($plugin_definition['views']['filter_plugins'][$type])
          ? $plugin_definition['views']['filter_plugins'][$type] : $plugin_definition['views']['filter_plugins']['standard'],
      ],
      'sort' => [
        'id' => !empty($plugin_definition['views']['sort_plugins'][$type])
          ? $plugin_definition['views']['sort_plugins'][$type] : $plugin_definition['views']['sort_plugins']['standard'],
      ],
    ];
  }
  else {
    $relationship_table_key = 'web_service_client:' . $service->id() . ':' . $type;
    $data[$table_key][$key] = [
      'title' => $key,
      'help' => $key,
      'relationship' => [
        'id' => $plugin_definition['views']['relationship_plugin'],
        'base' => $relationship_table_key,
        'web_service_client_extra' => $endpoint->getDefinitionExtraRelationship($key, $type),
        'title' => $key,
        'label' => $key,
        'help' => t('(No description available)'),
      ],
    ];
  }
}
