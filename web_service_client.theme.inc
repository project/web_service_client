<?php

/**
 * @file
 * Contains web_service_client.theme.inc.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * Themes the operation form for editing the used parameters.
 *
 * @ingroup themeable
 */
function theme_web_service_client_parameter_form($variables) {
  $elements = $variables['element'];

  $table['#theme'] = 'table';
  $table['#header'] = [
    t('Data type'),
    t('Multiple'),
    t('Name'),
    t('Default value'),
    t('Required'),
    ['data' => t('Weight'), 'class' => ['tabledrag-hide']],
  ];
  $table['#attributes']['id'] = 'rules-' . Html::getId($elements['#title']) . '-id';

  foreach (Element::children($elements['items']) as $key) {
    $element = &$elements['items'][$key];
    // Add special classes to be used for tabledrag.js.
    $element['weight']['#attributes']['class'] = ['rules-element-weight'];

    $row = [];
    $row[] = ['data' => $element['type']];
    $row[] = ['data' => $element['multiple']];
    $row[] = ['data' => $element['name']];
    $row[] = ['data' => $element['default_value']];
    $row[] = ['data' => $element['required']];
    $row[] = ['data' => $element['weight']];
    $row = ['data' => $row] + $element['#attributes'];
    $row['class'][] = 'draggable';
    $table['#rows'][] = $row;
  }
  $elements['items']['#printed'] = TRUE;
  unset($elements['items']);
  if (!empty($table['#rows'])) {
    $options = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'rules-element-weight',
      'table_id' => $table['#attributes']['id'],
    ];
    drupal_attach_tabledrag($table, $options);
  }

  // Theme it like a form item, but with the description above the content.
  $attributes['class'][] = 'form-item';
  $attributes['class'][] = 'rules-variables-form';

  $output = '<div' . new Attribute($attributes) . '>' . "\n";
  //$output .= theme('form_element_label', $variables);
  $vars = $variables;
  $vars['#theme'] = 'form_element_label';
  //$output .= render($vars);
  if (!empty($elements['#description'])) {
    $output .= ' <div class="description">' . $elements['#description'] . "</div>\n";
  }
  $output .= ' ' . render($table) . "\n";
  // Add in any further children elements.
  foreach (Element::children($elements, TRUE) as $key) {
    $output .= render($elements[$key]);
  }
  $output .= "</div>\n";
  return $output;
}

/**
 * Themes the global parameters form for editing the used parameters.
 *
 * @ingroup themeable
 */
function theme_web_service_client_global_parameter_form($variables) {
  $elements = $variables['element'];

  $table['#theme'] = 'table';
  $table['#header'] = [t('Name'), t('Default value')];
  $table['#attributes']['id'] = 'rules-' . Html::getId($elements['#title']) . '-id';

  foreach (Element::children($elements['items']) as $key) {
    $element = &$elements['items'][$key];
    // Add special classes to be used for tabledrag.js.
    $element['weight']['#attributes']['class'] = ['rules-element-weight'];

    $row = [];
    $row[] = ['data' => $element['name']];
    $row[] = ['data' => $element['default_value']];
    $row = ['data' => $row] + $element['#attributes'];
    $row['class'][] = 'draggable';
    $table['#rows'][] = $row;
  }
  $elements['items']['#printed'] = TRUE;
  unset($elements['items']);

  // Theme it like a form item, but with the description above the content.
  $attributes['class'][] = 'form-item';
  $attributes['class'][] = 'rules-variables-form';

  $output = '<div' . new Attribute($attributes) . '>' . "\n";
  //    $output .= theme('form_element_label', $variables);
  $vars = $variables;
  $vars['#theme'] = 'form_element_label';
  //$output .= render($vars);
  if (!empty($elements['#description'])) {
    $output .= ' <div class="description">' . $elements['#description'] . "</div>\n";
  }
  $output .= ' ' . render($table) . "\n";
  // Add in any further children elements.
  foreach (Element::children($elements, TRUE) as $key) {
    $output .= render($elements[$key]);
  }
  $output .= "</div>\n";
  return $output;
}


/**
 * Themes the data type form for editing the properties.
 *
 * @ingroup themeable
 */
function theme_web_service_client_property_form($variables) {
  $elements = $variables['element'];

  $table['#theme'] = 'table';
  $table['#header'] = [t('Data type'), t('Multiple'), t('Name'), t('Label')];
  $table['#attributes']['id'] = 'rules-' . Html::getId($elements['#title']) . '-id';

  foreach (Element::children($elements['items']) as $key) {
    $element = &$elements['items'][$key];

    $row = [];
    $row[] = ['data' => $element['type']];
    $row[] = ['data' => $element['multiple']];
    $row[] = ['data' => $element['name']];
    $row[] = ['data' => $element['label']];
    $row = ['data' => $row] + $element['#attributes'];
    $table['#rows'][] = $row;
  }
  $elements['items']['#printed'] = TRUE;
  unset($elements['items']);

  // Theme it like a form item, but with the description above the content.
  $attributes['class'][] = 'form-item';
  $attributes['class'][] = 'rules-variables-form';

  $output = '<div' . new Attribute($attributes) . '>' . "\n";
  //    $output .= theme('form_element_label', $variables);
  $vars = $variables;
  $vars['#theme'] = 'form_element_label';
  //$output .= render($vars);
  if (!empty($elements['#description'])) {
    $output .= ' <div class="description">' . $elements['#description'] . "</div>\n";
  }
  $output .= ' ' . render($table) . "\n";
  // Add in any further children elements.
  foreach (Element::children($elements, TRUE) as $key) {
    $output .= render($elements[$key]);
  }
  $output .= "</div>\n";
  return $output;
}

function theme_web_service_client_entity_ui_overview_item($variables) {
  $element = $variables['element'];
  $output = $element['#url'] ? Drupal::l($element['#label'], $element['#url']['path'], $element['#url']['options']) : SafeMarkup::checkPlain($element['#label']);
  if ($element['#name']) {
    $output .= ' <small> (' . t('Machine name') . ': ' . SafeMarkup::checkPlain($element['#name']) . ')</small>';
  }
  return $output;
}
